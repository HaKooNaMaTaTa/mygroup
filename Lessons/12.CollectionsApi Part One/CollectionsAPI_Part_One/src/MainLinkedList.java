import java.util.LinkedList;

public class MainLinkedList {
    public static void main(String[] args) {
        LinkedList<Integer> integers = new LinkedList<>();

        int i = 10;
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);

        integers.set(3, 25);

        System.out.println(integers.get(10));
        System.out.println(integers.getFirst());
        System.out.println(integers.getLast());
        integers.removeFirst();
        System.out.println(integers.getFirst());
        System.out.println(integers.getLast());
    }
}
