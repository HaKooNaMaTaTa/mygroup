import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestDeleteLists {
    public static void main(String[] args) {
        ArrayList<Integer> integerArrayList = new ArrayList<>();
        LinkedList<Integer> integerLinkedList = new LinkedList<>();
        addElementsInList(integerArrayList);
        addElementsInList(integerLinkedList);

        System.out.println("*** Удаление по индексу ***");
//        deleteElementOnIndexInArrayList(integerArrayList, 5_000_000);
//        deleteElementOnIndexInLinkedList(integerLinkedList, 5_000_000);
        deleteElementOnIndexInArrayList(integerArrayList, integerArrayList.size() - 1);
        deleteElementOnIndexInLinkedList(integerLinkedList, integerLinkedList.size() - 1);
        System.out.println("*** Удаление по значению ***");
//        deleteElementOnValueInArrayList(integerArrayList, 5_000_000);
//        deleteElementOnValueInLinkedList(integerLinkedList, 5_000_000);
        deleteElementOnValueInArrayList(integerArrayList, 0);
        deleteElementOnValueInLinkedList(integerLinkedList, 0);
    }

    public static void deleteElementOnIndexInArrayList(List<Integer> list, int index) {
        Date start = new Date();
        list.remove(index);
        Date end = new Date();
        System.out.println("ArrayList удалил элемент по индексу за - " + (end.getTime() - start.getTime()));
    }

    public static void deleteElementOnIndexInLinkedList(List<Integer> list, int index) {
        Date start = new Date();
        list.remove(index);
        Date end = new Date();
        System.out.println("LinkedList удалил элемент по индексу за - " + (end.getTime() - start.getTime()));
    }

    public static void deleteElementOnValueInArrayList(List<Integer> list, Integer value) {
        Date start = new Date();
        list.remove(value);
        Date end = new Date();
        System.out.println("ArrayList удалил элемент по значению за - " + (end.getTime() - start.getTime()));
    }

    public static void deleteElementOnValueInLinkedList(List<Integer> list, Integer value) {
        Date start = new Date();
        list.remove(value);
        Date end = new Date();
        System.out.println("LinkedList удалил элемент по значению за - " + (end.getTime() - start.getTime()));
    }

    public static void addElementsInList(List<Integer> list) {
        for (int i = 0; i < 10_000_000; i++) {
            list.add(i);
        }
    }
}
