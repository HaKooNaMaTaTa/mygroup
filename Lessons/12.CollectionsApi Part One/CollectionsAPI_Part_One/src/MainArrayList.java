import java.util.ArrayList;
import java.util.Iterator;

public class MainArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        int i = 10;
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);
        integers.add(i++);

        integers.set(3, 25);

//        integers.removeIf(integer -> integer % 2 == 0);

        System.out.println("Обход коллекции через цикл for");
        for (int j = 0; j < integers.size(); j++) {
            //System.out.println(array[j])
            System.out.print(integers.get(j) + ", ");
        }
        System.out.println();
        System.out.println("Обход коллекции через итератор");
        //Iterator - специальный объект, предназначенный для последовательно обхода по коллекции
        //TODO: как это выглядит физически?
        Iterator<Integer> integerIterator = integers.iterator();

        while(integerIterator.hasNext()) {
            System.out.print(integerIterator.next() + ", ");
        }
        System.out.println();

        System.out.println("Обход коллекции через for-each");
        for (Integer integer : integers) {
            System.out.print(integer + ", ");
        }
    }
}
