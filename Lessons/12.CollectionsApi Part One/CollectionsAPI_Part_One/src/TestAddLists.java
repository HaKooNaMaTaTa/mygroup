import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestAddLists {
    public static void main(String[] args) {
        ArrayList<Integer> arrayIntegers = new ArrayList<>();
        LinkedList<Integer> linkedIntegers = new LinkedList<>();

//        System.out.println("Добавление в конец");
//        addLastInList(arrayIntegers);
//        addLastInList(linkedIntegers);
        //Массив
        //[0], [1], [2], [3], [4], [5]
        //ArrayList
        //[-2], [-1], [0], [1], [2], [3], [4], [5]
//        System.out.println("Добавление в начало");
//        addBeginArrayList();
//        addBeginLinkedList();

        System.out.println("Добавление в середину");
        addInMiddleArrayList();
        addInMiddleLinkedList();
    }

    public static void addLastInList(List<Integer> integers) {
        Date start = new Date();
        for (int i = 0; i < 1_000_000; i++) {
            integers.add(i);
        }
        Date end = new Date();
        System.out.println("Элементы были добавлены в конец за - " + (end.getTime() - start.getTime()));
    }

    public static void addBeginLinkedList() {

        LinkedList<Integer> integers = new LinkedList<>();
        Date start = new Date();
        for (int i = 0; i < 1_000_000; i++) {
            integers.addFirst(i);
        }
        Date end = new Date();
        System.out.println("Элементы были добавлены в начало за - " + (end.getTime() - start.getTime()));
    }

    //01 >> 1 - 001
    public static void addBeginArrayList() {
        ArrayList<Integer> integers = new ArrayList<>();
        Date start = new Date();
        for (int i = 0; i < 1_000_000; i++) {
            //[0]
            //[2], [1], [0]
            integers.add(0, i);
        }
        Date end = new Date();
        System.out.println("Элементы были добавлены в начало за - " + (end.getTime() - start.getTime()));
    }

    public static void addInMiddleArrayList() {
        ArrayList<Integer> integers = new ArrayList<>();
        Date start = new Date();

        for (int i = 0; i < 1_000_000; i++) {
            integers.add(integers.size() / 2, i);
        }
        Date end = new Date();
        System.out.println("Элементы были добавлены в середину за - " + (end.getTime() - start.getTime()));
    }

    public static void addInMiddleLinkedList() {
        LinkedList<Integer> integers = new LinkedList<>();
        Date start = new Date();

        for (int i = 0; i < 1_000_000; i++) {
            integers.add(integers.size() / 2, i);
        }
        Date end = new Date();
        System.out.println("Элементы были добавлены в середину за - " + (end.getTime() - start.getTime()));
    }
}
