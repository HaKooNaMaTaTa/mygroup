package com.maxima.springboot.controller;

import com.maxima.springboot.dto.SignUpForm;
import com.maxima.springboot.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Controller
@RequestMapping(value = "/signUp") //указали на какой url будет реагировать данный контроллер
public class SignUpController {

    private final AccountService accountService;

    @Autowired
    public SignUpController(AccountService accountService) {
        this.accountService = accountService;
    }

    //    @GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String getSignUpPage(Model model) {
        model.addAttribute("signUpForm", new SignUpForm());
        return "signUp";
    }

    //@PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String signUp(@Valid SignUpForm signUpForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("signUpForm", signUpForm);
            return "signUp";
        }
        accountService.saveAccount(signUpForm);
        return "redirect:/signIn";
    }
}
