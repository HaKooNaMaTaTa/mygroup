package com.maxima.springboot.service;

import com.maxima.springboot.dto.SignUpForm;
import com.maxima.springboot.model.Account;
import com.maxima.springboot.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveAccount(SignUpForm signUpForm) {
        Account account = Account.builder()
                .firstName(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail())
                .password(signUpForm.getPassword())
                .build();

        accountRepository.save(account);
    }
}
