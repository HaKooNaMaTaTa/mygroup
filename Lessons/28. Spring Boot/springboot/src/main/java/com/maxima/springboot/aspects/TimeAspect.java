package com.maxima.springboot.aspects;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect //В данном классе будет реализована "сквозная" логика
@Component
public class TimeAspect {

	//Аннотация @Before говорит нам, что данный Advice будет выполняться ДО метода оригинала
	// и только в тех методах у которых тип возвращаемого значения совпадает с указанным в скобках
	// в данном случае стоит * - значит любой тип возвращаемого значения
	@Around(value = "execution(* com.maxima.springboot.controller..*(..))")
	public Object testMethod() {
		System.out.println("Всем привет, мы в АОПляндии");
		return null;
	}
}
