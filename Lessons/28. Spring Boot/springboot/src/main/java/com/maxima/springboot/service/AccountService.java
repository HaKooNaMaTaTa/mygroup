package com.maxima.springboot.service;


import com.maxima.springboot.dto.SignUpForm;


public interface AccountService {

    void saveAccount(SignUpForm signUpForm);

}
