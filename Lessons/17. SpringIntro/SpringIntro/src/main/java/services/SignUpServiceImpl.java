package services;

import models.Account;
import repository.AccountRepository;
import validators.BlackListPasswords;
import validators.EmailValidator;
import validators.PasswordValidator;

public class SignUpServiceImpl implements SignUpService {

    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    private final BlackListPasswords blackListPasswords;
    private final AccountRepository accountRepository;

    public SignUpServiceImpl(EmailValidator emailValidator,
                             PasswordValidator passwordValidator,
                             BlackListPasswords blackListPasswords,
                             AccountRepository accountRepository) {
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
        this.blackListPasswords = blackListPasswords;
        this.accountRepository = accountRepository;
    }

    @Override
    public void save(Account account) {
        blackListPasswords.contains(account.getPassword());
        emailValidator.validate(account.getEmail());
        passwordValidator.validate(account.getPassword());

        accountRepository.save(account);
    }
}
