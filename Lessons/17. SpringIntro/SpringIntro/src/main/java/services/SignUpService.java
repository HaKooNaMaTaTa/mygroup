package services;

import models.Account;

public interface SignUpService {

    void save(Account account);
}
