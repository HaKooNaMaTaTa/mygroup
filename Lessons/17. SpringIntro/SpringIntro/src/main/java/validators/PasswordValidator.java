package validators;

public interface PasswordValidator {
    boolean validate(String password);
}
