package validators;

public interface BlackListPasswords {

    boolean contains(String password);
}
