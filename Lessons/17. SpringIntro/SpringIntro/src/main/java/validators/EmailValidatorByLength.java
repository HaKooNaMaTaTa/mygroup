package validators;

public class EmailValidatorByLength implements EmailValidator {

    private int minLength;

    public EmailValidatorByLength(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean validate(String email) {
        if (email.length() < minLength) {
            throw new IllegalArgumentException("Email введен некорректно. Проверьте email.");
        }

        return true;
    }
}
