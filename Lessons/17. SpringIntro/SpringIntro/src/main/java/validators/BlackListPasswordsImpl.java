package validators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlackListPasswordsImpl implements BlackListPasswords {

    private final List<String> badPasswords = new ArrayList<>(Arrays.asList("admin", "qwerty", "12345"));
    @Override
    public boolean contains(String password) {
        if (badPasswords.contains(password)) {
            throw new IllegalArgumentException("Ваш пароль - уже слит в интернет. Придумайте другой");
        }

        return true;
    }
}
