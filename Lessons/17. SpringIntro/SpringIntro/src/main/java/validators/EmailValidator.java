package validators;

public interface EmailValidator {

    boolean validate(String email);
}
