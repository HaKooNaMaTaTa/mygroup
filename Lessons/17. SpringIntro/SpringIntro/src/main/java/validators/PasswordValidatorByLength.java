package validators;

public class PasswordValidatorByLength implements PasswordValidator {

    private int minLength;

    public PasswordValidatorByLength(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean validate(String password) {
        if (password.length() < minLength) {
            throw new IllegalArgumentException("Пароль слишком короткий");
        }
        return true;
    }
}
