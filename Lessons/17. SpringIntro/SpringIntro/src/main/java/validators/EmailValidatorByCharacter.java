package validators;

import java.util.regex.Pattern;

public class EmailValidatorByCharacter implements EmailValidator {

    //Объект данного класса принимает в себя, при создании, некий шаблон
    //и мы можем с помощью этого шаблона проверять строки
    //соответствуют ли они этому шаблону
    private Pattern pattern;

    //oleg@mail.com
    @Override
    public boolean validate(String email) {
        //Проверяет строку на соответствие шаблону
        //Если строка не соответствует, то выкидываем исключение
        //В нашем случае - почта введена без '@'
        if (!pattern.matcher(email).find()) {
            throw new IllegalArgumentException("Email введен некорректно. Проверьте email.");
        }

        return true;
    }

    public void setPattern(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }
}
