package validators;

public class PasswordValidatorByCharacter implements PasswordValidator {
    @Override
    public boolean validate(String password) {
        if (password.indexOf('!') == -1 || password.indexOf('@') == -1 || password.indexOf('#') == -1) {
            throw new IllegalArgumentException("Пароль не содержит спецсимволы - !, @, #");
        }

        return true;
    }
}
