package solid;

import services.SignUpService;
import services.SignUpServiceImpl;

public class DIP {
    //Модули верхних уровней не должны зависеть от модулей нижних уровней
    private SignUpServiceImpl signUpService;
}
