package solid;

public class LSP {


    class CalculateSomething {

        public int calculate() {return 2 + 2;}
    }

    class CalculateLength extends CalculateSomething {
        @Override
        public int calculate() {
            return 2 + 2;
        }
    }
}
