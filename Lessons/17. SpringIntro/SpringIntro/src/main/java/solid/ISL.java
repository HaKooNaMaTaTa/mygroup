package solid;

import models.Account;

public interface ISL {

    void printHello();
    void printBye();

    int calculateInt();
    double calculateDouble();

    Account getAccount();
    void saveAccount();
}
