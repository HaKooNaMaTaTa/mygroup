package app;

import models.Account;
import repository.AccountRepository;
import repository.AccountRepositoryImpl;
import services.SignUpService;
import services.SignUpServiceImpl;
import validators.BlackListPasswords;
import validators.BlackListPasswordsImpl;
import validators.EmailValidator;
import validators.EmailValidatorByCharacter;
import validators.EmailValidatorByLength;
import validators.PasswordValidator;
import validators.PasswordValidatorByCharacter;
import validators.PasswordValidatorByLength;

public class Main {
    public static void main(String[] args) {
        EmailValidatorByCharacter emailValidator = new EmailValidatorByCharacter();
        emailValidator.setPattern(".+@.+");
        PasswordValidator passwordValidator = new PasswordValidatorByCharacter();
        BlackListPasswords blackListPasswords = new BlackListPasswordsImpl();
        AccountRepository accountRepository = new AccountRepositoryImpl();

        SignUpService signUpService = new SignUpServiceImpl(
                emailValidator,
                passwordValidator,
                blackListPasswords,
                accountRepository);

        signUpService.save(Account.builder()
                .firstName("Oleg")
                .lastName("Igonin")
                .email("oleg@mail.com")
                .password("qwerty!@#45")
                .build());
    }
}
