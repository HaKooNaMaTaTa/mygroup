package repository;

import models.Account;

public interface AccountRepository {

    void save(Account account);
}
