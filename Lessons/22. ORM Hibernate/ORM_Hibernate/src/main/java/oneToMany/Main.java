package oneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

//            Airplane airplane = Airplane.builder()
//                    .name("Boeing 777")
//                    .build();
//
//            Passenger passenger1 = Passenger.builder()
//                    .firstName("Oleg")
//                    .lastName("Igonin")
//                    .airPlane(airplane)
//                    .build();
//
//            Passenger passenger2 = Passenger.builder()
//                    .firstName("Igor")
//                    .lastName("Igonin")
//                    .airPlane(airplane)
//                    .build();
//
//            Passenger passenger3 = Passenger.builder()
//                    .firstName("Vladimir")
//                    .lastName("Igonin")
//                    .airPlane(airplane)
//                    .build();
//
//            airplane.setPassengers(Arrays.asList(passenger1, passenger2, passenger3));
//
//            session.save(airplane);
//            session.save(passenger1);
//            session.save(passenger2);
//            session.save(passenger3);

            Airplane airplane = session.get(Airplane.class, 1L);
            System.out.println(airplane);
        }
    }
}
