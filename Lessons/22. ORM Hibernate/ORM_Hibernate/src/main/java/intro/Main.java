package intro;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

//            Car car = Car.builder()
//                    .brand("Lada")
//                    .model("Vesta")
//                    .color("Black")
//                    .build();
//
//            session.save(car);
//
//            car = Car.builder()
//                    .brand("Lada")
//                    .model("Priora")
//                    .color("Green")
//                    .build();
//
//            session.save(car);
            Car car = session.get(Car.class, 3L);
            System.out.println(car);
        }
    }
}
