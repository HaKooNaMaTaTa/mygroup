package intro;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//POJO - Plain Old Java Object:
//1. Класс не должен быть конечным (не должно быть модикатора final)
//2. Поля класса не должны быть final
//3. Обязательно должен быть пустой конструктор (при этом, могут быть и другие конструкторы)
//4. Поля должны быть приватными, доступ к ним - через геттеры и сеттеры
//5. У класса обязательно должны быть две аннотации - @Entity и @Id
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String brand;
    private String model;
    private String color;
}
