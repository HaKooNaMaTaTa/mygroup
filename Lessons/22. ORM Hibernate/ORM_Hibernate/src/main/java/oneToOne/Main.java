package oneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {
//            Passport passport = Passport.builder()
//                    .series("2222")
//                    .number("3333333")
//                    .build();
//
//            Person person = Person.builder()
//                    .firstName("Igor")
//                    .lastName("Igonin")
//                    .patronymic("Leopoldovich")
//                    .passport(passport)
//                    .build();
//
//            session.save(passport);
//            session.save(person);

            Person person = session.get(Person.class, 1L);
            System.out.println(person);
            //TODO: ленивая иницализация - прокси?
            Passport passport = person.getPassport();
            System.out.println(person.getPassport().getNumber());
            int i = 0;
        }
    }
}
