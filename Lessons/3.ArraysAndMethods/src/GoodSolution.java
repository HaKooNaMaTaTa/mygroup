import java.util.Scanner;

public class GoodSolution {
    //void - пустота
    public static void main(String[] args) {
        //Синтаксис массива
        //ТИП ДАННЫХ[] НАИМЕНОВАНИЕ ПЕРЕМЕННОЙ = new ТИП ДАННЫХ[РАЗМЕР]
        //Оператор new - отвечает за выделение памяти
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int[] array1 = new int[count]; // == int[] array1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
//        int array2[] = new int[10]; //моветон - это дурной тон
//        int[] array3 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        //[0],[0],[0],[0],[0]
        /*
        1. массивы это ссылочная структура данных - переменная ссылается на самый первый, нулевой элемент
            Мы хотим вытащить элемент под индексом(адресом) 5
            Берется адрес в памяти самого первого, нулевого элемента и к нему прибавляется число, которое мы с вами указали в скобках
        2. в скобках, при объявлении массива, мы обазательно должны указать значение
        3. Отсчет в массиве идет с нуля
        4. При работе с массивом нужно различать две вещи - индекс и элемент
         0,  1,  2,  3,  4,  5,  6,  7,  8,   9
        {11, 25, 30, 54, 25, 16, 7, 68, 909, 1110}
        5. Длина массива - это значение, которое мы с вами указали в квадратных скобках при создании
        А последний индекс массива ВСЕГДА вычисляется по формуле ДЛИНА_МАССИВА - 1
         */

        int[] array2 = createIntArray(count);
        int[] array3 = createIntArray(12);
        int[] array4 = createIntArray(15);

        int number = findIndexByValue(array2, 102);
        findIndexByValueVoid(array2, 102);
//        createIntArray(10);
//        createIntArray(12);
//        createIntArray(13);
//        //0.863 * 1000 = 863
//        for (int i = 0; i < array1.length; i = i + 1) {
//            array1[i] = (int)(Math.random() * 1000);
//        }
//
//        int[] array2 = new int[count];
//
//        for (int i = 0; i < array1.length; i = i + 1) {
//            array2[i] = (int)(Math.random() * 1000);
//        }
//
//        int[] array3 = new int[count];
//
//        for (int i = 0; i < array1.length; i = i + 1) {
//            array3[i] = (int)(Math.random() * 1000);
//        }

        int j = 0;
    }

    //Именнованный блок кода, который можно выполнить - это метод
    //Методы, условно, делятся на два типа - функции и процедуры
    //Процедура - это метод, который явно ничего не возвращает
    //Функция - метод, который возвращает результат своей работы

    //СИНТАКСИС МЕТОДА
    //МОДИФИКАТОР_ДОСТУПА СТАТИК/НЕ_СТАТИК ТИП_ВОЗВРАЩАЕМОГО_ЗНАЧЕНИЯ НАИМЕНОВАНИЕ_МЕТОДА(ФОРМАЛЬНЫЕ ПАРАМЕТРЫ) {
    //...Блок кода
    //}
    //в аргументы метода всегда все передается по значению, а не по ссылке
    public static int[] createIntArray(int count) {
        count = count * 2;
        int[] array = new int[count];
        for (int i = 0; i < array.length; i = i + 1) {
            array[i] = (int)(Math.random() * 1000);
        }

        return array;
    }

    public static int findIndexByValue(int[] array, int number) {
        for (int i = 0; i < array.length; i = i + 1) {
            if (array[i] == number) {
                return i;
            }
        }

        System.out.println("Такого числа нет в массиве");
        return -1;
    }

    public static void findIndexByValueVoid(int[] array, int number) {
        for (int i = 0; i < array.length; i = i + 1) {
            if (array[i] == number) {
                System.out.println("Число - " + number + " находится под индексом - " + i);
                return; //оператор выхода из метода
            }
        }

        System.out.println("Такого числа нет в массиве");
    }
}
