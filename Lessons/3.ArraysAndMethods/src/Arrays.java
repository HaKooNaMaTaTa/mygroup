import java.util.Scanner;

public class Arrays {
    //Задача - найти наибольшее число из предоставленных
    public static void main(String[] args) {
        //Благодаря этой строчке мы можем вводить данные с клавиатуры в программу
        Scanner scanner = new Scanner(System.in);
        System.out.println("Пожалуйста, введите первое число");
        //Объявляется number1 и в нее кладется число, которые мы введем на клавиатуре
        int number1 = scanner.nextInt();
        System.out.println("Пожалуйста, введите второе число");
        //Объявляется number2 и в нее кладется число, которые мы введем на клавиатуре
        int number2 = scanner.nextInt();

        //Если (if переводится как - "если") условие в скобках верно, то мы
        //"проваливаемся/заходим" внутрь фигурных скобок, если ложно - то мы
        //переходим к следующему условию, если и оно ложно - то мы переходим в
        //блок "else", который выполнится в том случае, если все условия до него - ложны
        if (number1 > number2) {
            System.out.println("Первое число больше");
        } else if (number2 > number1) {
            System.out.println("Второе число больше");
        } else {
            System.out.println("Оба числа равны");
        }
    }
}
