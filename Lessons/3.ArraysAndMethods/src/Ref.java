import java.util.Arrays;

public class Ref {
    public static void main(String[] args) {
        int num = 10; //??? Integer
        increment(num);
        System.out.println(num);

        int[] array = {10, 20, 30, 40, 50};
        incrementArray(array);
        System.out.println(Arrays.toString(array));
    }

    public static void increment(int num) {
        num = num + 1;
    }

    public static void incrementArray(int[] array) {
        for (int i = 0; i < array.length; i = i + 1) {
            array[i] = array[i] + 1;
        }
    }
}
