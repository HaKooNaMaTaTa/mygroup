import java.util.Scanner;

public class BadSolution {
    //Задача - найти наибольшее число из предоставленных
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int number1 = scanner.nextInt();
//        int number2 = scanner.nextInt();
//        int number3 = scanner.nextInt();
//        int number4 = scanner.nextInt();
//        int number5 = scanner.nextInt();
//        int number6 = scanner.nextInt();
//        int number7 = scanner.nextInt();
//        int number8 = scanner.nextInt();
//        int number9 = scanner.nextInt();
//        int number10 = scanner.nextInt();
//
//        //Блок гавнокода, который находит наибольшее число
//    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice = 1; //нужна для того, что бы остановить программу
        int result = 0; //данная переменная будет хранить наибольшее число
        while (choice == 1) {
            int number1 = scanner.nextInt();
            int number2 = scanner.nextInt();

            //ИЛИ (В Java выглядит так - ||)
            /*
                1 1 = 1
                0 1 = 1
                1 0 = 1
                0 0 = 0
             */
            //И (В Java выглядит так - &&)
            /*
                1 1 = 1
                0 1 = 0
                1 0 = 0
                0 0 = 0
             */
            if (number1 > number2 && number1 > result) {
                result = number1;
            } else if (number2 > number1 && number2 > result) {
                result = number2;
            }

            System.out.println("Продолжаем ввод чисел? Если да - то введите 1, если нет - то -1 ");
            choice = scanner.nextInt();
        }

        System.out.println("Наибольшее число - " + result);
    }
}