package reflection.documents;

import reflection.documents.framework.DefaultValue;

import java.time.LocalDate;

public class Act {

    private LocalDate date;
    private String name;
    private String description;

    @DefaultValue("Игонин Олег")
    private String form;

    public Act(LocalDate date, String name, String description) {
        this.date = date;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Act{" +
                "date=" + date +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", form='" + form + '\'' +
                '}';
    }
}
