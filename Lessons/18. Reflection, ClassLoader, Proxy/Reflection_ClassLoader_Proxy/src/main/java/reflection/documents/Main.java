package reflection.documents;

import reflection.documents.framework.DocumentFramework;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
//        Act act = new Act(LocalDate.now(), "Сводка по материалам", "Смета на материалы для стройки");
//        Statement statement = new Statement("Igonin Oleg", LocalDate.of(1996, 8, 22));
        DocumentFramework documentFramework = new DocumentFramework();

        Act act = documentFramework.generate(
                Act.class,
                LocalDate.now(),
                "Сводка по материалам",
                "Смета на материалы для стройки");
        Statement statement = documentFramework.generate(
                Statement.class,
                "Igonin Oleg",
                LocalDate.of(1996, 8, 22));
        System.out.println(act);
        System.out.println(statement);
    }
}
