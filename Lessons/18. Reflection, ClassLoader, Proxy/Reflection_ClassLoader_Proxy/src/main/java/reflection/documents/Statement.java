package reflection.documents;

import reflection.documents.framework.DefaultValue;

import java.time.LocalDate;

public class Statement {

    private String name;
    private LocalDate birthDate;

    @DefaultValue("ООО Рога и Копыта")
    private String company;

    @DefaultValue("1234567890")
    private String inn;

    public Statement(String name, LocalDate birthDate) {
        this.name = name;
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Statement{" +
                "name='" + name + '\'' +
                ", birthDate=" + birthDate +
                ", company='" + company + '\'' +
                ", inn='" + inn + '\'' +
                '}';
    }
}
