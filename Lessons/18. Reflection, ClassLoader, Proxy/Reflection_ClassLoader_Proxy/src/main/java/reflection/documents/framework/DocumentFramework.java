package reflection.documents.framework;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class DocumentFramework {

    //Данный метод умеет создавать объекты на основе класса и аргументов, которые ему передали
    //args - это значения, которые нам нужно положить
    public <T> T generate(Class<T> clazz, Object ... args) {
        //Для того, что бы выбрать нужный конструктор, нам нужно передать конструкторы
        //ТИПЫ (а не значение) аргументов, которые у нас есть
        //Создаем список, который будет содержать классы аргументов, которые мы передали
        //Список содержит объекты с информацией о классах пераднных нам аргументов
        //String name = "Oleg;
        //String - это класс (тип данных)
        // name - наименование переменной
        // "Oleg" - значение этой переменной -> "Oleg".getClass();
        List<Class<?>> argsType = new ArrayList<>();
        for (Object obj : args) {
            argsType.add(obj.getClass());
        }
        //{LocalDate.class, String.class, String.class}
        //Из списка мы делаем массив
        Class<?>[] argsTypeToArray = new Class[argsType.size()];
        //[LocalDate.class, String.class, String.class]
        argsType.toArray(argsTypeToArray);
        //Теперь у нас есть информация по количеству параметров конструктора и типам параметров
        try {
            Constructor<T> constructor = clazz.getConstructor(argsTypeToArray);
            T document = constructor.newInstance(args);
            processDefaultValue(document);
            return document;
        } catch (ReflectiveOperationException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private <T> void processDefaultValue(T document) {
        Class<T> clazz = (Class<T>) document.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            DefaultValue defaultValue = field.getAnnotation(DefaultValue.class);
            if (defaultValue != null) {
                try {
                    String value = defaultValue.value();
                    field.setAccessible(true);
                    field.set(document, value);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
