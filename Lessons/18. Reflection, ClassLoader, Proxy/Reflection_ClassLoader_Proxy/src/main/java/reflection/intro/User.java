package reflection.intro;

public class User {

    private String firstName;
    private String lastName;
    private String patronymic;

    public User(String firstName) {
        this.firstName = firstName;
    }

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(String firstName, String lastName, String patronymic) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public void say(String message) {
        System.out.println(firstName + " say: " + message);
    }

}
