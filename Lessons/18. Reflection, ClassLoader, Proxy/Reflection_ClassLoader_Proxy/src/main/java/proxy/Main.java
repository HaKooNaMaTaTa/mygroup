package proxy;

import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        InputOutput inputOutput = new InputOutputImpl("file.txt");
        inputOutput = LoggerWrapper.withLogger(inputOutput);
        inputOutput.output("Hi! ");
        inputOutput.output("How are you? ");
        inputOutput.output("I`m fine \n");
        System.out.println(inputOutput.input());
    }
}
