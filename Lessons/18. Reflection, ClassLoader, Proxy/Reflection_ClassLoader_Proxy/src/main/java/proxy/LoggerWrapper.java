package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalDateTime;

public class LoggerWrapper {

    private static class InputOutputInvocationHandler implements InvocationHandler {

        private final InputOutput original;

        public InputOutputInvocationHandler(InputOutput original) {
            this.original = original;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println(LocalDateTime.now() + " был вызван метод - " + method.getName() + " " + (args != null ? args[0] : null));
            return method.invoke(original, args);
        }
    }

    public static InputOutput withLogger(InputOutput original) {
        //1. Нам нужно взять тот же загрузчик классов, что и у оригинала
        ClassLoader classLoader = original.getClass().getClassLoader();
        //2. Нам нужно взять те же интерфейсы, что и у оригинала
        Class<?>[] interfaces = original.getClass().getInterfaces();
        return (InputOutput) Proxy.newProxyInstance(classLoader, interfaces, new InputOutputInvocationHandler(original));
    }
}
