package classloader;

import java.io.FileInputStream;
import java.io.InputStream;

//Создаем свой загрузчик
//Наследуемся от Абстрактного класса ClassLoader
//И переопределяем метод findClass
public class FolderClassLoader extends ClassLoader {
    //Переменная, которая хранит путь до папки, где лежат нужные нам классы
    private String folderName;

    public FolderClassLoader(String folderName) {
        this.folderName = folderName;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            //Добавляем к имени класса расширение
            String pathToClass = folderName + "/" + name + ".class";
            //Получаем наш класс в виде массива байтов и отдаем его нативным методам для загрузки, настройки и инициализации
            try (InputStream inputStream = new FileInputStream(pathToClass)){
                byte[] bytes = new byte[inputStream.available()];
                int bytesCount = inputStream.read(bytes);
                return defineClass(name, bytes, 0, bytesCount);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
