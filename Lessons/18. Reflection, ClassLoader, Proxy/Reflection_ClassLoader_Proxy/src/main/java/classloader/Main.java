package classloader;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println(User.class.getClassLoader());
        System.out.println(String.class.getClassLoader());

        Class<?> exampleClass = Class.forName("Example");

        System.out.println(exampleClass.getClassLoader());
    }
}
