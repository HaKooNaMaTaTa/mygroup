package polymorphism;

public class SuperSportCar extends SportCar {

    private boolean spoiler;

    public SuperSportCar(String brand, String model, boolean isTurbo, boolean spoiler) {
        super(brand, model, isTurbo);
        this.spoiler = spoiler;
    }

    public boolean isSpoiler() {
        return spoiler;
    }

    public void setSpoiler(boolean spoiler) {
        this.spoiler = spoiler;
    }
}
