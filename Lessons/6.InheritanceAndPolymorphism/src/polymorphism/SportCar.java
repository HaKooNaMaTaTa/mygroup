package polymorphism;

public class SportCar extends Car {

    private boolean isTurbo;
    private boolean isNitro;

    public SportCar(String brand, String model, boolean isTurbo) {
        super(brand, model);
        this.isTurbo = isTurbo;
        this.isNitro = false;
    }

    public void switchNitro() {
        isNitro = !isNitro; //инвертируем значение переменной isNitro
    }

    @Override
    public void drive() {
        if (isTurbo) {
            System.out.println("Мы в спорткаре. МЫ ЛЕЕЕЕТИИИИИИИМ");
        } else {
            System.out.println("Мы в спорткаре и нам очень грустно");
        }
    }

    public boolean isTurbo() {
        return isTurbo;
    }

    public void setTurbo(boolean turbo) {
        isTurbo = turbo;
    }

    public boolean isNitro() {
        return isNitro;
    }

    public void setNitro(boolean nitro) {
        isNitro = nitro;
    }
}
