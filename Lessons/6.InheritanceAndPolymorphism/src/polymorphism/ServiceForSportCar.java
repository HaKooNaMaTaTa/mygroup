package polymorphism;

public class ServiceForSportCar {

    public void tuning(SportCar sportCar) {
        if (sportCar.isTurbo()) {
            System.out.println("Турбо режим уже есть, тюнинговать нечего");
        } else {
            sportCar.setTurbo(true);
            System.out.println("Турбо режим установлен, не гоняйте на дорогах!");
        }
    }
}
