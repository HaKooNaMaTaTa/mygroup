package polymorphism;

public class OffRoadCar extends Car {

    private boolean isFullWheel;

    public OffRoadCar(String brand, String model, boolean isFullWheel) {
        super(brand, model);
        this.isFullWheel = isFullWheel;
    }

    @Override
    public void drive() {
        //super.drive(); //Если super без скобок - значит это ссылка на (в данном случае) метод класса предка
        if (isFullWheel) {
            System.out.println("У нас полный привод, мы едем по азимуту");
        } else {
            System.out.println("У нас нет полного привода, плак-плак:С");
        }
    }

    public boolean isFullWheel() {
        return isFullWheel;
    }

    public void setFullWheel(boolean fullWheel) {
        isFullWheel = fullWheel;
    }
}
