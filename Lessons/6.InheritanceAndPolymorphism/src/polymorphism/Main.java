package polymorphism;


public class Main {
    //**Восходящее преобразование** - это когда в объектную переменную класса-предка кладется ссылка на объект
    //класса-потомка
    public static void main(String[] args) {
//        SportCar car = new SportCar("Ferrari", "Enzo", true);
//
//        System.out.println(car.getBrand());
//        System.out.println(car.getModel());
//        car.drive();

        ServiceForSportCar service = new ServiceForSportCar();

        OffRoadCar offRoadCar = new OffRoadCar("Subaru", "Forester", true);
        CitizenCar citizenCar = new CitizenCar("Lada", "Vesta", true);
        SportCar sportCar = new SportCar("Lada", "2113", false);
        SuperSportCar superSportCar  = new SuperSportCar("DAEWOOE", "NEXIA", false, true);

        service.tuning(sportCar);
        service.tuning(superSportCar);

//        System.out.println(offRoadCar.getClass().getName());
//        System.out.println(offRoadCar.getClass().getSimpleName());
//        System.out.println(offRoadCar.getClass().getCanonicalName());
//        System.out.println(offRoadCar.getClass().getTypeName());
    }
}
