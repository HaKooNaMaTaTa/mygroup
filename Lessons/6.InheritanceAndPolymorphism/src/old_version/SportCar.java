package old_version;

public class SportCar {
    private String brand;
    private String model;
    private boolean isTurbo;

    public SportCar(String brand, String model, boolean isTurbo) {
        this.brand = brand;
        this.model = model;
        this.isTurbo = isTurbo;
    }

    public void drive() {
        if (isTurbo) {
            System.out.println("Мы в спорткаре. МЫ ЛЕЕЕЕТИИИИИИИМ");
        } else {
            System.out.println("Мы в спорткаре и нам очень грустно");
        }
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isTurbo() {
        return isTurbo;
    }

    public void setTurbo(boolean turbo) {
        isTurbo = turbo;
    }
}
