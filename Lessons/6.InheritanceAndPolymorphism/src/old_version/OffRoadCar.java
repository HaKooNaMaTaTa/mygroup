package old_version;

public class OffRoadCar {
    private String brand;
    private String model;
    private boolean isFullWheel;

    public OffRoadCar(String brand, String model, boolean isFullWheel) {
        this.brand = brand;
        this.model = model;
        this.isFullWheel = isFullWheel;
    }

    public void drive() {
        if (isFullWheel) {
            System.out.println("У нас полный привод, мы едем по азимуту");
        } else {
            System.out.println("У нас нет полного привода, плак-плак:С");
        }
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isFullWheel() {
        return isFullWheel;
    }

    public void setFullWheel(boolean fullWheel) {
        isFullWheel = fullWheel;
    }
}
