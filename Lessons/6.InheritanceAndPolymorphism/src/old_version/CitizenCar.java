package old_version;

public class CitizenCar {
    private String brand;
    private String model;
    private boolean trunk; //багажник на крыше

    public CitizenCar(String brand, String model, boolean trunk) {
        this.brand = brand;
        this.model = model;
        this.trunk = trunk;
    }

    public void drive() {
        if (trunk) { //trunk == true trunk != true trunk == false
            System.out.println("Мы засунули все вещи в багажник. Мы едем в путешествие");
        } else {
            System.out.println("Нам некуда положить вещи. Мы никуда не едем");
        }
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isTrunk() {
        return trunk;
    }

    public void setTrunk(boolean trunk) {
        this.trunk = trunk;
    }
}
