package old_version;

public class Main {

    public static void main(String[] args) {
        CitizenCar citizenCar = new CitizenCar("Lada", "Vesta", true);
        SportCar sportCar = new SportCar("Lada", "2113", false);
        OffRoadCar offRoadCar = new OffRoadCar("Subaru", "Forester", true);

        citizenCar.drive();
        sportCar.drive();
        offRoadCar.drive();
    }
}
