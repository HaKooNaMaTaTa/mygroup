package inheritance;

public class SportCar extends Car {

    private boolean isTurbo;

    public SportCar(String brand, String model, boolean isTurbo) {
        super(brand, model);
        this.isTurbo = isTurbo;
    }

    @Override
    public void drive() {
        if (isTurbo) {
            System.out.println("Мы в спорткаре. МЫ ЛЕЕЕЕТИИИИИИИМ");
        } else {
            System.out.println("Мы в спорткаре и нам очень грустно");
        }
    }

    public boolean isTurbo() {
        return isTurbo;
    }

    public void setTurbo(boolean turbo) {
        isTurbo = turbo;
    }
}
