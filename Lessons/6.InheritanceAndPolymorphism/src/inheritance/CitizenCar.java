package inheritance;

//extends - оператор наследования
//слева от оператора класс наследник, справа - класс предок
public class CitizenCar extends Car {

    private boolean trunk;

    public CitizenCar(String brand, String model, boolean trunk) {
        super(brand, model);
        this.trunk = trunk;
    }

    public void drive() {
        if (trunk) { //trunk == true trunk != true trunk == false
            System.out.println("Мы засунули все вещи в багажник. Мы едем в путешествие");
        } else {
            System.out.println("Нам некуда положить вещи. Мы никуда не едем");
        }
    }

    public boolean isTrunk() {
        return trunk;
    }

    public void setTrunk(boolean trunk) {
        this.trunk = trunk;
    }
}
