package inheritance;

public class Main {

    public static void main(String[] args) {
        Car car = new Car("AbstractBrand", "AbstractModel");
        CitizenCar citizenCar = new CitizenCar("Lada", "Vesta", true);
        SportCar sportCar = new SportCar("Lada", "2113", false);
        OffRoadCar offRoadCar = new OffRoadCar("Subaru", "Forester", true);

        car.drive();
        citizenCar.drive();
        sportCar.drive();
        offRoadCar.drive();
    }
}
