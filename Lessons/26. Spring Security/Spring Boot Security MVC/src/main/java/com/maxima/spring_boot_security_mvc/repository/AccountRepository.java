package com.maxima.spring_boot_security_mvc.repository;

import com.maxima.spring_boot_security_mvc.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

	List<Account> findAllByLastName(String lastName);

	//Таблица User, поле integration_some_attribute_id as integ_id
	//select integ_id
	@Query("select acc from Account acc where acc.email = :email")
	List<Account> fBE(@Param("email") String email);

	Optional<Account> findByEmail(String email);
}
