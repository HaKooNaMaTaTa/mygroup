package com.maxima.spring_boot_security_mvc.controller;

import com.maxima.spring_boot_security_mvc.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @GetMapping
    public String getListAccounts(Model model) {
        model.addAttribute("accounts", accountService.getAllAccounts());
        return "accounts";
    }

    // url/accounts/2
    @RequestMapping(value = "/{account_id}", method = RequestMethod.GET)
    public String getAccount(Model model, @PathVariable(value = "account_id") Long id) {
        model.addAttribute("account", accountService.getById(id));
        return "account";
    }
}
