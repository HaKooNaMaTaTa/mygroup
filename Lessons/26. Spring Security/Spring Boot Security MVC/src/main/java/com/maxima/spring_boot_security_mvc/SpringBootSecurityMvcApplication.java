package com.maxima.spring_boot_security_mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSecurityMvcApplication.class, args);
	}

}
