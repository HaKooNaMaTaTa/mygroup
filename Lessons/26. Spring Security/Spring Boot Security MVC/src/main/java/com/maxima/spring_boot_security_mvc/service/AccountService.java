package com.maxima.spring_boot_security_mvc.service;


import com.maxima.spring_boot_security_mvc.dto.AccountDto;
import com.maxima.spring_boot_security_mvc.dto.SignInForm;
import com.maxima.spring_boot_security_mvc.model.Account;

import java.util.List;

public interface AccountService {

    void saveAccount(AccountDto accountDto);

    List<Account> getAllAccounts();

    Boolean signIn(SignInForm form);

    Account getById(Long id);
}
