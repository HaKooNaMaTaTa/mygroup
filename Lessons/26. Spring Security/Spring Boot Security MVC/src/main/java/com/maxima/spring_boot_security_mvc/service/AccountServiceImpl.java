package com.maxima.spring_boot_security_mvc.service;

import com.maxima.spring_boot_security_mvc.dto.AccountDto;
import com.maxima.spring_boot_security_mvc.dto.SignInForm;
import com.maxima.spring_boot_security_mvc.model.Account;
import com.maxima.spring_boot_security_mvc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void saveAccount(AccountDto accountDto) {
        Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail())
                .password(passwordEncoder.encode(accountDto.getPassword()))
                .role(Account.Role.USER)
                .state(Account.State.NOT_CONFIRMED)
                .build();

        accountRepository.save(account);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    //TODO: Нужно написать дополнительный метод в accountRepository, который будет искать аккаунты в бд по email и паролю
    @Override
    public Boolean signIn(final SignInForm form) {
        //Берем все аккаунты из бд
        List<Account> accounts = accountRepository.findAll();
        //Находим аккаунт, у которой такое же мыло и такой же пароль
        Optional<Account> account = accounts.stream()
                .filter(acc -> acc.getEmail().equals(form.getEmail()) && acc.getPassword().equals(form.getPassword()))
                .findFirst();
        //Если он существует - вернется true, если нет - false
        return account.isPresent();
    }

    @Override
    public Account getById(Long id) {
        return accountRepository.findById(id).get();
    }
}
