package interface_segregation;

public class NumberUtil implements BadInterface {
    @Override
    public void sumNumbers(int... numbers) {

    }

    @Override
    public void multipleNumbers(int... numbers) {

    }

    @Override
    public void powerTV() {

    }

    @Override
    public void walkOnDog() {

    }
}
