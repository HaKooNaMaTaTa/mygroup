package interface_segregation;

public interface BadInterface {

    void sumNumbers(int ... numbers);
    void multipleNumbers(int ... numbers);
    void powerTV();
    void walkOnDog();
}
