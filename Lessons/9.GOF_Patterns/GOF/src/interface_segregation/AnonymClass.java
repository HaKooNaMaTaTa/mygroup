package interface_segregation;

import patterns.behavioral.strategy.PayStrategy;

public class AnonymClass implements PayStrategy {
    @Override
    public void pay(int cost) {
        System.out.println(100 - cost);
    }
}
