package interface_segregation;

import patterns.behavioral.strategy.Cash;
import patterns.behavioral.strategy.PayStrategy;

public class Main {

    public static void main(String[] args) {
//       patterns.behavioral.strategy.PayStrategy payStrategy = new Cash(100);
        //Функциональные интерфейсы можно заменить лямбда-выражением
        //Лямбда выражение - реализация ОДНОГО абстрактного метода функционального интерфейса
        //cost -> System.out.println(100 - cost) <- это реализация метода Pay
        //1. Создается анонимный класс (класс без названия)
        //2. Анонимный имплементирует функциональный интерфейс
        //3. В качестве реализации метода функ. интерфейса берется (в метод засовывается) строка
        // cost -> System.out.println(100 - cost)
        //4. Создается объект анонимного класса и передается в метод

        //Синтаксис:
        //-> - само лямбда выражение
        //то, что слева - это аргументы абстрактного метода, реализацию которого мы сейчас пишем
       pay(cost -> {
           int balance = 100;
           System.out.println("Подключаюсь к банку карты");
           System.out.println("Идет подключение... 1");
           System.out.println("Идет подключение... 2");
           System.out.println("Идет подключение... 3");
           if (balance >= cost) {
               balance -= cost; // balance = balance - cost;
               System.out.println("Оплата успешно произведена!");
           } else {
               System.out.println("Недостаточно средств на карте");
           }
       }, 50);
    }

    public static void pay(PayStrategy payStrategy, int cost) {
        payStrategy.pay(cost);
        payStrategy.getBalance();
    }
}
