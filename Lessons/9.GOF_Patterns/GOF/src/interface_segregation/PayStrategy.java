package interface_segregation;

//Интерфейс варианта оплаты
//Он имеет один абстрактный (без реализации) метод - это функциональный интерфейс
public interface PayStrategy {

    void pay(int cost);

    default int getBalance() {
        return 100;
    }
}
