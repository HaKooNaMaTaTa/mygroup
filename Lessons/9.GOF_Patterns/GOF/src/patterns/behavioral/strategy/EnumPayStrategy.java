package patterns.behavioral.strategy;

public enum EnumPayStrategy {
    CASH, CREDIT_CARD, PAYPAL, APPLE_PAY
}
