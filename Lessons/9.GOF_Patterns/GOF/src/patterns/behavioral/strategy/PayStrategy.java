package patterns.behavioral.strategy;

//Интерфейс варианта оплаты
public interface PayStrategy {

    void pay(int cost);
    default int getBalance() {
        return 100;
    }
}
