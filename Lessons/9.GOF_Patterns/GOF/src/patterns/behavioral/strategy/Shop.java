package patterns.behavioral.strategy;

public class Shop {

    private Product[] products;

    public Shop(Product ... products) {
        this.products = products;
    }

    public void sell(Client client) {
        int finalCost = 0;

        for (int i = 0; i < client.getProducts().length; i++) {
            finalCost += client.getProducts()[i].getCost();
        }

        client.getPayStrategy().pay(finalCost);
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }
}
