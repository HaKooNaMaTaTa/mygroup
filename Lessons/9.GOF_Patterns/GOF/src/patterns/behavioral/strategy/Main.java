package patterns.behavioral.strategy;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Client client = new Client("Oleg");
        Shop shop = new Shop(
                new Product(10, "Mountain Dew"),
                new Product(20, "Pepsi"),
                new Product(30, "Coca Cola"),
                new Product(40, "Iron Brew"),
                new Product(50, "Очаковский")
        );

        client.setProducts(
                shop.getProducts()[0],
                shop.getProducts()[1],
                shop.getProducts()[2],
                shop.getProducts()[3],
                shop.getProducts()[4]
        );

        System.out.println("Как будете расплачиваться?");
        String choice = scanner.nextLine();

        //.equalsIgnoreCase()
        /*
            .equals() -> CARD card
            .equalsIgnoreCase() -> CARD card Card cArd
         */
        if (choice.equalsIgnoreCase(EnumPayStrategy.CASH.name())) {
            client.setPayStrategy(new Cash(160));
        }

        if (choice.equalsIgnoreCase(EnumPayStrategy.CREDIT_CARD.name())) {
            client.setPayStrategy(new CreditCard(160));
        }

        if (choice.equalsIgnoreCase(EnumPayStrategy.PAYPAL.name())) {
            client.setPayStrategy(new PayPal(160));
        }
        System.out.println("Давайте расплачиваться");
        shop.sell(client);
    }
}
