package patterns.behavioral.strategy;

public class CreditCard implements PayStrategy {

    private int balance;

    public CreditCard(int balance) {
        this.balance = balance;
    }

    @Override
    public void pay(int cost) {
        System.out.println("Подключаюсь к банку карты");
        System.out.println("Идет подключение... 1");
        System.out.println("Идет подключение... 2");
        System.out.println("Идет подключение... 3");
        if (balance >= cost) {
            balance -= cost; // balance = balance - cost;
            System.out.println("Оплата успешно произведена!");
        } else {
            System.out.println("Недостаточно средств на карте");
        }
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
