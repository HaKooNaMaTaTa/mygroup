package patterns.behavioral.strategy;

public class PayPal implements PayStrategy {

    private int balance;

    public PayPal(int balance) {
        this.balance = balance;
    }

    @Override
    public void pay(int cost) {
        System.out.println("Подключаюсь к облаку");
        System.out.println("Идет подключение... 1");
        System.out.println("Идет подключение... 2");
        System.out.println("Идет подключение... 3");
        if (balance >= cost) {
            balance -= cost; // balance = balance - cost;
            System.out.println("Оплата успешно произведена!");
        } else {
            System.out.println("Недостаточно средств на облаке");
        }
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
