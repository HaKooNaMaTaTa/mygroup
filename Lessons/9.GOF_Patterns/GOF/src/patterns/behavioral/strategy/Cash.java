package patterns.behavioral.strategy;

public class Cash implements PayStrategy {

    private int cash;

    public Cash(int cash) {
        this.cash = cash;
    }

    @Override
    public void pay(int cost) {
        System.out.println("Ищем деньги в кармане");
        if (cash >= cost) {
            cash -= cost; // balance = balance - cost;
            System.out.println("Оплата успешно произведена!");
        } else {
            System.out.println("В кармане недостаточно денег");
        }
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }
}
