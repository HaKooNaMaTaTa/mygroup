package patterns.creational.singleton;

public class Main {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance("Это паттерн Одиночка!");
        Singleton singleton2 = Singleton.getInstance("Это паттерн Singleton!");

        System.out.println(singleton1.getMessage());
        System.out.println(singleton2.getMessage());

    }
}
