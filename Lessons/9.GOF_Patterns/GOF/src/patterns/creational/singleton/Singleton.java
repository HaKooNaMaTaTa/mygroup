package patterns.creational.singleton;

//Проблема, которую решает данный паттерн - это контроль количества объектов данного класса
public class Singleton {

    private String message;
    private static Singleton singleton;

    private Singleton(String message) {
        this.message = message;
    }

    public static Singleton getInstance(String message) {
        if (singleton == null) {
            singleton = new Singleton(message);
        }
        return singleton;
    }

    public String getMessage() {
        return message;
    }
}
