package app.questiuon_about_join;

public class Car {

    private Long id;
    private String brand;
    private String model;
    private String color;

    private Engine engine;

}

// car - id, brand, model, color
// engine - id, v, power, name, car_id

//select * from car where id = 2;
//Car {id=2, brand="Audi", model="A6, color="Black", engine = null}

//select * from car where id = 2 left join engine on engine.car_id = car.id ;
//Car {id=2, brand="Audi", model="A6, color="Black", engine = null}