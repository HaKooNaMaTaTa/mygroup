package app.main;

import app.model.Car;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
    public static void main(String[] args) throws SQLException {
        //Соединение с базой данных.
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "postgres");

        //Statement, PreparedStatement и CallableStatement
        Statement statement = connection.createStatement();

        //Statement - транспорт нашего тоннеля. Получает запрос и едет с ним в БД.
        //ResultSet - хранилище ответа БД на наш запрос. Т.е. statement привез ответ от БД и положил в ResultSet

        //В данном объекте будет лежать вся информация по нашему запросу.
        ResultSet result = statement.executeQuery("select * from car");

        //.next() - это и .hasNext(), и .next() в одном флаконе. Если получилось перепрыгнуть - он возвращает true
        // и указывает на строку, которую перепрыгнул
        while (result.next()) {
            Long id = result.getLong(1);
            String brand = result.getString(2);
            String model = result.getString(3);
            String color = result.getString(4);
            Long driverId = result.getLong(5);

            Car car = Car.builder()
                    .id(id)
                    .brand(brand)
                    .model(model)
                    .color(color)
                    .driverId(driverId)
                    .build();

            System.out.println(car);
        }


        int i = 0;
    }
}
