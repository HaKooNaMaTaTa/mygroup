package app.main;

import app.model.Car;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class InjMain {

    public static final String FIND_ALL_CARS = "select * from car";
    public static final String FIND_BY_ID = "select * from car where id=";

    public static void main(String[] args) throws SQLException {
        //Соединение с базой данных.
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "postgres");

        //Statement, PreparedStatement и CallableStatement
        Statement statement = connection.createStatement();
        Scanner scanner = new Scanner(System.in);
        String id = scanner.nextLine();
        //В данном объекте будет лежать вся информация по нашему запросу.
//        ResultSet result = statement.executeQuery(FIND_ALL_CARS);
        ResultSet result = statement.executeQuery(FIND_BY_ID + id);

        //.next() - это и .hasNext(), и .next() в одном флаконе. Если получилось перепрыгнуть - он возвращает true
        // и указывает на строку, которую перепрыгнул
        while (result.next()) {
            Long carId = result.getLong(1);
            String brand = result.getString(2);
            String model = result.getString(3);
            String color = result.getString(4);
            Long driverId = result.getLong(5);

            Car car = Car.builder()
                    .id(carId)
                    .brand(brand)
                    .model(model)
                    .color(color)
                    .driverId(driverId)
                    .build();

            System.out.println(car);
        }
    }
}
//Вместо запроса select * from car where id=2 -> select * from car where id=2; drop table car;
//EV245YH; drop schema public