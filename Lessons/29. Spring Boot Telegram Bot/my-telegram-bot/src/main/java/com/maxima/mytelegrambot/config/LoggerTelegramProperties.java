package com.maxima.mytelegrambot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "log-bot")
public class LoggerTelegramProperties {

	private String name;
	private String token;
	private String projectName;
}
