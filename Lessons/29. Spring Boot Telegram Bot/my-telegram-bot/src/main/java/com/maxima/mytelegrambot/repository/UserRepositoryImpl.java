package com.maxima.mytelegrambot.repository;

import com.maxima.mytelegrambot.model.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserRepositoryImpl implements UserRepository {

	public static final List<User> USERS = new ArrayList<>();
	@Override
	public List<User> findAll() {
		return USERS;
	}

	@Override
	public Optional<User> findById(Long id) {
		return USERS.stream().filter(user -> user.getId().equals(id)).findFirst();
	}

	@Override
	public void save(User user) {
		USERS.add(user);
	}
}
