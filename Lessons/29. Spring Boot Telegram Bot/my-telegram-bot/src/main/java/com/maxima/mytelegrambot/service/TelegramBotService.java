package com.maxima.mytelegrambot.service;

import com.maxima.mytelegrambot.config.LoggerTelegramProperties;
import com.maxima.mytelegrambot.model.User;
import com.maxima.mytelegrambot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TelegramBotService extends TelegramLongPollingBot {

	private final LoggerTelegramProperties loggerTelegramProperties;
	private final UserRepository userRepository;

	@Autowired
	public TelegramBotService(LoggerTelegramProperties loggerTelegramProperties, UserRepository userRepository) {
		this.loggerTelegramProperties = loggerTelegramProperties;
		this.userRepository = userRepository;
	}

	@Override
	public String getBotUsername() {
		return loggerTelegramProperties.getName();
	}

	@Override
	public String getBotToken() {
		return loggerTelegramProperties.getToken();
	}

	//Что делать, когда нам пишет пользователь?
	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage() && !update.getMessage().getText().isEmpty()) {
			String message;
			if (update.getMessage().getText().equals("Поприветствуй меня")) {
				Optional<User> userOptional = userRepository.findById(update.getMessage().getChatId());
				if (!userOptional.isPresent()) {
					User user = new User();
					user.setId(update.getMessage().getChatId());
					user.setFirstName(update.getMessage().getChat().getFirstName());
					user.setLastName(update.getMessage().getChat().getLastName());
					user.setRole(User.Role.ADMIN);

					userRepository.save(user);
				}
				String firstName = update.getMessage().getChat().getFirstName();
				String lastName = update.getMessage().getChat().getLastName();
				String bio = update.getMessage().getChat().getBio();
				Long id = update.getMessage().getChat().getId();

				message = "Hi! " + firstName + " " + lastName +
						". Your bio - " + bio + ", your id - " + id;
			} else if (update.getMessage().getText().equals("Попращайся со мной")) {
				message = "Good Bye my Dear Master";
			} else {
				message = " я не понимать, что ты хотеть";
			}

			SendMessage sendMessage = new SendMessage(update.getMessage().getChatId().toString(), message);
			ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();

			List<KeyboardRow> rows = new ArrayList<>();

			KeyboardRow keyboardRow = new KeyboardRow();
			keyboardRow.add("Поприветствуй меня");
			keyboardRow.add("Попращайся со мной");
			rows.add(keyboardRow);

			markup.setKeyboard(rows);

			sendMessage.setReplyMarkup(markup);
			try {
				execute(sendMessage);
			} catch (TelegramApiException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
