package com.maxima.mytelegrambot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@Configuration
@EnableJpaRepositories(basePackages = "com.maxima.mytelegrambot.repository")
@SpringBootApplication
public class MyTelegramBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyTelegramBotApplication.class, args);
	}

}
