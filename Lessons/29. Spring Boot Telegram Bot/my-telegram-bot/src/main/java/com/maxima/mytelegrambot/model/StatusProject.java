package com.maxima.mytelegrambot.model;

public enum StatusProject {
	LAUNCHED, STOPPED
}
