package com.maxima.mytelegrambot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

	@Id
	private Long id;
	private String firstName;
	private String lastName;
	private Role role;


	public enum Role {
		ADMIN, USER
	}
}
