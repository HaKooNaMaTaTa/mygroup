package lesson_controller.controller;

import lesson_controller.dto.AccountDto;
import lesson_controller.dto.SignInForm;
import lesson_controller.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/signIn") //указали на какой url будет реагировать данный контроллер
@RequiredArgsConstructor
public class SignInController {

    private final AccountService accountService;

//    @GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String getSignInPage() {
        return "signIn";
    }

//    @PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String signIn(SignInForm form) {
        if (accountService.signIn(form)) {
            return "redirect:/accounts";
        } else {
            return "redirect:/signUp?error";
        }
    }
}
