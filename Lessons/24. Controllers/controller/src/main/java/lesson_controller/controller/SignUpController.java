package lesson_controller.controller;

import lesson_controller.dto.AccountDto;
import lesson_controller.model.Account;
import lesson_controller.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/signUp") //указали на какой url будет реагировать данный контроллер
public class SignUpController {

    private final AccountService accountService;

    @Autowired
    public SignUpController(AccountService accountService) {
        this.accountService = accountService;
    }

    //    @GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String getSignUpPage() {
        return "signUp";
    }

    //@PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String signUp(AccountDto dto) {
        accountService.saveAccount(dto);
        return "redirect:/signIn";
    }
}
