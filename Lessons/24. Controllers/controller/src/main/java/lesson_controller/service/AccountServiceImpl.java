package lesson_controller.service;

import lesson_controller.dto.AccountDto;
import lesson_controller.dto.SignInForm;
import lesson_controller.model.Account;
import lesson_controller.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveAccount(AccountDto accountDto) {
        Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail())
                .password(accountDto.getPassword())
                .build();

        accountRepository.save(account);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.getAllAccounts();
    }

    //TODO: Нужно написать дополнительный метод в accountRepository, который будет искать аккаунты в бд по email и паролю
    @Override
    public Boolean signIn(final SignInForm form) {
        //Берем все аккаунты из бд
        List<Account> accounts = accountRepository.getAllAccounts();
        //Находим аккаунт, у которой такое же мыло и такой же пароль
        Optional<Account> account = accounts.stream()
                .filter(acc -> acc.getEmail().equals(form.getEmail()) && acc.getPassword().equals(form.getPassword()))
                .findFirst();
        //Если он существует - вернется true, если нет - false
        return account.isPresent();
    }

    @Override
    public Account getById(Long id) {
        List<Account> accounts = accountRepository.getAllAccounts();
        //Находим аккаунт, у которой такое же мыло и такой же пароль
        Optional<Account> account = accounts.stream()
                .filter(acc -> acc.getId() == id)
                .findFirst();

        return account.orElse(Account.builder()
                .firstName("")
                .lastName("")
                .email("")
                .build());
    }
}
