package lesson_controller.service;

import lesson_controller.dto.AccountDto;
import lesson_controller.dto.SignInForm;
import lesson_controller.model.Account;

import java.util.List;

public interface AccountService {

    void saveAccount(AccountDto accountDto);

    List<Account> getAllAccounts();

    Boolean signIn(SignInForm form);

    Account getById(Long id);
}
