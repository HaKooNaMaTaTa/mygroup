package lesson_controller.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HiberSessionUtil implements DataBaseHiber {

    private final SessionFactory sessionFactory;

    //TODO
    //Я забыл сконфигурировать конфигурацию (т.е. не вызвал метод .configure())
    //И поэтому была пустая конфигурация и хибер не подтягивался корректно
    @Autowired
    public HiberSessionUtil() {
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
    }

//    //Метод, помеченный данной аннотацией, выполнится всего один раз. После того
//    //как будет создан бин данного класса
//    @PostConstruct
//    private void init() {
//        //Если значение TRUE, то сессия свободна, если FALSE - занята
//        mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
//        mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
//        mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
//        mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
//        mapSession.put(sessionFactory.openSession(), Boolean.TRUE);
//    }

    @Override
    public Session getSession() {
        return sessionFactory.openSession();
    }
}
