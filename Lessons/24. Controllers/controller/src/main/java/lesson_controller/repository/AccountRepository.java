package lesson_controller.repository;

import lesson_controller.model.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository {

    void save(Account account);

    List<Account> getAllAccounts();
}
