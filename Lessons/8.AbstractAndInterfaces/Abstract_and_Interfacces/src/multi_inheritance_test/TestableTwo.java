package multi_inheritance_test;

public interface TestableTwo {

    default void test() {
        System.out.println("Это тестовый интерфейс метода номер 2");
    }
}
