package multi_inheritance_test;

public interface MainTestable extends TestableOne, TestableTwo {
    @Override
    default void test() {
        System.out.println("Это реализация главного интерфейса");
    }
}
