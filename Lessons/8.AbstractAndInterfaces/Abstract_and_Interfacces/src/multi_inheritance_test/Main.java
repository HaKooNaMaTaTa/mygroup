package multi_inheritance_test;

public class Main {
    public static void main(String[] args) {
//        TestableOne testableOne = new Test();
        TestableTwo testableTwo = new Test();
        //MainTestable mainTestable = new Test();

        testableTwo.test();
//        testableOne.test();
        //mainTestable.test();
    }
}
