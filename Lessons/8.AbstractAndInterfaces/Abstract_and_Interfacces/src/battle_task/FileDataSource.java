package battle_task;

import java.util.Random;
import java.util.UUID;

public class FileDataSource implements DataSource {

    private boolean connection;

    public FileDataSource() {
        connectToDataSource();
    }

    @Override
    public boolean connectToDataSource() {
        System.out.println("Connecting... 1");
        System.out.println("Connecting... 2");
        System.out.println("Connecting... 3");
        System.out.println("Connection successful");
        connection = true;


        return connection;
    }

    @Override
    public Data createData(String message) {
        Data data = null;
        if (connection) {
            data = new Data(UUID.randomUUID(), message);
        }
        return data;
    }

    @Override
    public Data update(UUID id, Data data) {
        if (connection) {
            Data oldData = read(id);
            oldData = data;
            System.out.println("Данные сохранены");

            return oldData;
        }
        return null;
    }

    @Override
    public Data read(UUID id) {
        System.out.println("Залезли в бд и нашли по указанному id нужный объект");
        return new Data(id, "Message");
    }

    @Override
    public void delete(UUID id) {
        System.out.println("Мы нашли Data по указанному id удалили");
    }
}
