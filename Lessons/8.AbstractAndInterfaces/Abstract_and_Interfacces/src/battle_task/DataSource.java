package battle_task;

import java.util.UUID;

//Интерфейс для работы с данными
public interface DataSource {

    boolean connectToDataSource();
    //CRUD - Create, Read, Update, Delete
    Data createData(String message);

    Data update(UUID id, Data data);

    Data read(UUID id);

    void delete(UUID id);
}
