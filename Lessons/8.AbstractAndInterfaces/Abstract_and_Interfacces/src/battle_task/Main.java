package battle_task;

//SOLID
public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DataBaseDataSource();
//        FileDataSource fileDataSource = new FileDataSource();
//        DataBaseDataSource dataBaseDataSource = new DataBaseDataSource();

        Data data = dataSource.createData("Hello world!");
        Data newData = dataSource.update(data.getId(), data);
        Data newData2 = dataSource.read(data.getId());
        dataSource.delete(data.getId());

    }
}
