package battle_task;

import java.util.Random;
import java.util.UUID;

public class DataBaseDataSource implements DataSource {

    private boolean connection;

    public DataBaseDataSource() {
        connectToDataSource();
    }

    @Override
    public boolean connectToDataSource() {
        Random random = new Random();
        int flaq = random.nextInt(10);
        System.out.println("Connecting... 1");
        System.out.println("Connecting... 2");
        System.out.println("Connecting... 3");
        if (flaq < 5) {
            System.out.println("Connection successful");
            connection = true;
        } else {
            System.out.println("Connection error, please see logs");
            connection = false;
        }

        return connection;
    }

    @Override
    public Data createData(String message) {
        Data data = null;
        if (connection) {
            data = new Data(UUID.randomUUID(), message);
        }
        return data;
    }

    @Override
    public Data update(UUID id, Data data) {
        if (connection) {
            Data oldData = read(id);
            oldData = data;
            System.out.println("Данные сохранены");

            return oldData;
        }
        return null;
    }

    @Override
    public Data read(UUID id) {
        System.out.println("Залезли в бд и нашли по указанному id нужный объект");
        return new Data(id, "Message");
    }

    @Override
    public void delete(UUID id) {
        System.out.println("Мы нашли Data по указанному id удалили");
    }
}
