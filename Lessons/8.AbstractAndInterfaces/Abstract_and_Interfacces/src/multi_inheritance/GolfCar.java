package multi_inheritance;

//public class GolfCar extends Car, Mechanism {
public class GolfCar implements Workable {

//    @Override
//    public void drive() {
//        System.out.println("Я еду на лунку номер 7");
//    }

    @Override
    public void transport() {
        System.out.println("Я перевожу клюшки");
    }

    @Override
    public void work() {
        System.out.println("Я маленький гольфкар и я работаю");
    }
}
