package multi_inheritance;

public interface Workable extends Transportable, Driveable {

    void work();
}
