package multi_inheritance;

public interface Driveable {

    default void drive() {
        System.out.println("Это реализация по умолчанию. Мы куда то едем");
    }
}
