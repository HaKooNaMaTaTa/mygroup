package multi_inheritance;

public interface Transportable {

    void transport();
}
