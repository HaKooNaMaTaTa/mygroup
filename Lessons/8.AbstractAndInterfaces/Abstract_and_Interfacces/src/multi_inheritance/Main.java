package multi_inheritance;

public class Main {
    public static void main(String[] args) {
        GolfCar golfCar = new GolfCar();

        golfCar.work();
        golfCar.drive();
        golfCar.transport();
    }
}
