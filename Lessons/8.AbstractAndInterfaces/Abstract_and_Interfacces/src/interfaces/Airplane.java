package interfaces;

//implements - "наследуемся" от интерфейса. Но правильнее имплементируем или реализуем интерфейс
public class Airplane implements Flyable {
    @Override
    public void fly() {
        System.out.println("Я самолет и я лечу вверх!");
    }
}
