package interfaces;

public interface Flyable {

    int MAX = 22;
    void fly();
}
