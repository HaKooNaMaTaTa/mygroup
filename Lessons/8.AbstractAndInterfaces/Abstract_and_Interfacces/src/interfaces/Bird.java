package interfaces;

public class Bird implements Flyable {

    @Override
    public void fly() {
        System.out.println("Чик чирик, я свил гнездо");
    }
}
