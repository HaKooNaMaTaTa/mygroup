package interfaces;

public class Main {
    public static void main(String[] args) {
        Airplane airplane = new Airplane();
        Bird bird = new Bird();
        Fanera fanera = new Fanera();
        Table table = new Table();
        Kukuha kukuha = new Kukuha();

//        airplane.fly();
//        bird.fly();
//        fanera.fly();
//        kukuha.fly();

        somebodyFly(airplane);
        somebodyFly(fanera);
        somebodyFly(kukuha);
        somebodyFly(bird);

    }

    public static void somebodyFly(Flyable flyable) {
        flyable.fly();
    }
}
