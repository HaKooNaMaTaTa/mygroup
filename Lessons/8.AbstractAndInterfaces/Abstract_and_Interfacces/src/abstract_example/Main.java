package abstract_example;

public class Main {
    public static void main(String[] args) {
//        Car car = new Car();
        SportCar sportCar = new SportCar("Brand", "Model", true);
        OffRoadCar offRoadCar = new OffRoadCar("Brand", "Model", true);
        CitizenCar citizenCar = new CitizenCar("Brand", "Model");

        //Не смотря на то, что класс Car является абстрактным, он может
        //иметь объектную переменную, которая будет ссылать на
        //объекты НЕ АБСТРАКТНЫХ классов наследников
        Car car = new OffRoadCar("Brand", "Model", true);

        car.drive();
        sportCar.drive();
        offRoadCar.drive();
        citizenCar.drive();
    }
}
