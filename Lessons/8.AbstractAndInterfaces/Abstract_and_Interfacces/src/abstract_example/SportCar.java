package abstract_example;

public class SportCar extends Car {

    private boolean isTurbo;

    public SportCar(String brand, String model, boolean isTurbo) {
        super(brand, model);
        this.isTurbo = isTurbo;
    }

    @Override
    public void drive() {
        System.out.println("Я спорткар, я хочу делать ВЖЖЖЖЖЖЖЖЖЖЖЖЖЖЖЖУУУУ");
    }

    public boolean isTurbo() {
        return isTurbo;
    }

    public void setTurbo(boolean turbo) {
        isTurbo = turbo;
    }
}
