package abstract_example;

//ключевое слово abstract - запрещает классу иметь объекты
public abstract class Car {

    private String brand;
    private String model;

    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    /*
        Если abstract стоит в сигнатуре метода, то в классе, в котором он объявлен,
        не нужно определять поведение этого метода. Это обязаны будут сделать классы-наследники.
        Если в классе есть хотя бы один абстрактный метод (метод без реализации), то класс тоже должен
        быть абстрактным.
     */
    public abstract void drive();

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
