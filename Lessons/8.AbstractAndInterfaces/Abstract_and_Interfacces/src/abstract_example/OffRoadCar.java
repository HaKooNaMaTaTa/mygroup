package abstract_example;

public class OffRoadCar extends Car {

    private boolean isFullWheels;

    public OffRoadCar(String brand, String model, boolean isFullWheels) {
        super(brand, model);
        this.isFullWheels = isFullWheels;
    }

    @Override
    public void drive() {
        System.out.println("Я джип, я еду куда хочу!");
    }

    public boolean isFullWheels() {
        return isFullWheels;
    }

    public void setFullWheels(boolean fullWheels) {
        isFullWheels = fullWheels;
    }
}
