package com.maxima.demo_test_telegram_bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTestTelegramBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoTestTelegramBotApplication.class, args);
	}

}
