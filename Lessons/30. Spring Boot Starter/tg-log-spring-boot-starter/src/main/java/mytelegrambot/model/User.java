package mytelegrambot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

	private Long id;
	private String firstName;
	private String lastName;
	private Role role;


	public enum Role {
		ADMIN, USER
	}
}
