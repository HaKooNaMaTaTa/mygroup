package mytelegrambot.repository;

import mytelegrambot.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository {

	List<User> findAll();

	Optional<User> findById(Long id);

	void save(User user);
}
