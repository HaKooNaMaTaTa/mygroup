package mytelegrambot.config;

import mytelegrambot.service.TelegramBotService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@EnableConfigurationProperties(TelegramLoggerProperties.class)
public class AutoConfigurationTelegramLogger {

	@Bean
	public TelegramBotService telegramBotService(TelegramLoggerProperties properties, Environment environment) {
		return new TelegramBotService(properties, environment);
	}
}
