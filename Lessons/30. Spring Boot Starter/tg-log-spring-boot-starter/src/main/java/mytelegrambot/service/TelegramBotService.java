package mytelegrambot.service;

import mytelegrambot.config.TelegramLoggerProperties;
import mytelegrambot.model.StatusProject;
import mytelegrambot.model.User;
import mytelegrambot.repository.UserRepository;
import mytelegrambot.repository.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TelegramBotService extends TelegramLongPollingBot {

	private final TelegramLoggerProperties telegramLoggerProperties;
	private final UserRepository userRepository = new UserRepositoryImpl();
	private final Environment properties;

	@Autowired
	public TelegramBotService(TelegramLoggerProperties telegramLoggerProperties, Environment properties) {
		this.telegramLoggerProperties = telegramLoggerProperties;
		this.properties = properties;
	}

	@Override
	public String getBotUsername() {
		return telegramLoggerProperties.getName();
	}

	@Override
	public String getBotToken() {
		return telegramLoggerProperties.getToken();
	}

	//Что делать, когда нам пишет пользователь?
	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage() && !update.getMessage().getText().isEmpty()) {
			String message;
			if (update.getMessage().getText().equals("Поприветствуй меня")) {
				Optional<User> userOptional = userRepository.findById(update.getMessage().getChatId());
				if (!userOptional.isPresent()) {
					User user = new User();
					user.setId(update.getMessage().getChatId());
					user.setFirstName(update.getMessage().getChat().getFirstName());
					user.setLastName(update.getMessage().getChat().getLastName());
					int number = (int) (Math.random() * 100);
					user.setRole(number > 50 ? User.Role.ADMIN : User.Role.USER);

					userRepository.save(user);
				}
				String firstName = update.getMessage().getChat().getFirstName();
				String lastName = update.getMessage().getChat().getLastName();
				String bio = update.getMessage().getChat().getBio();
				Long id = update.getMessage().getChat().getId();

				message = "Hi! " + firstName + " " + lastName +
						".\n Your bio - " + bio + "\n Your id - " + id;
			} else if (update.getMessage().getText().equals("Попращайся со мной")) {
				message = "Good Bye my Dear Master";
			} else {
				message = " я не понимать, что ты хотеть";
			}

			SendMessage sendMessage = new SendMessage(update.getMessage().getChatId().toString(), message);
			ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();

			List<KeyboardRow> rows = new ArrayList<>();

			KeyboardRow keyboardRow = new KeyboardRow();
			keyboardRow.add("Поприветствуй меня");
			keyboardRow.add("Попращайся со мной");
			rows.add(keyboardRow);

			markup.setKeyboard(rows);

			sendMessage.setReplyMarkup(markup);
			try {
				execute(sendMessage);
			} catch (TelegramApiException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@PostConstruct
	private void sendStatusPostStart() {
		sendMessage(StatusProject.LAUNCHED);
	}

	@PreDestroy
	private void sendStatusPreDestroy() {
		sendMessage(StatusProject.STOPPED);
	}

	private void sendMessage(StatusProject statusProject) {
		List<User> users = userRepository.findAll();
		String message = String.format("Проект - %s \n Статус - %s \n Время запуска - %s",
				properties.getProperty("log-bot.project-name"),
				statusProject,
				LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		users.stream()
				.filter(user -> user.getRole().equals(User.Role.ADMIN))
				.forEach(user -> {
					SendMessage sendMessage = new SendMessage(user.getId().toString(), message);
					try {
						execute(sendMessage);
					} catch (TelegramApiException e) {
						throw new RuntimeException(e);
					}
				});
	}
}
