public class OLogN {

    public static void main(String[] args) {
        int[] array = {2, 6, 7, 24, 66, 89, 100};

        System.out.println(recursiveBinarySearch(array, 66, 0, array.length - 1));
    }

    public static int recursiveBinarySearch(int[] array, int elementToSearch, int start, int end) {
        if (end >= start) {
            int mid = start + (end - start) / 2;

            if (array[mid] == elementToSearch) {
                return mid;
            }

            if (array[mid] > elementToSearch) {
                return recursiveBinarySearch(array, elementToSearch, start, mid - 1);
            }

            if (array[mid] < elementToSearch) {
                return recursiveBinarySearch(array, elementToSearch, mid + 1, end);
            }
        }

        return -1;
    }
}
