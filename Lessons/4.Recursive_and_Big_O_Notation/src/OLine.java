public class OLine {

    public static void main(String[] args) {
        //               0   1   2   3   4   5
        int[] numbers = {14, 16, 54, 78, 91, 34};
        System.out.println(findIndexElement(numbers, 1));
    }

    //Поиск индекса числа
    public static int findIndexElement(int[] numbers, int number) {
        for (int i = 0; i < numbers.length; i++) { // == i = i + 1
            if (numbers[i] == number) {
                return i;
            }
        }

        return -1;
    }
}
