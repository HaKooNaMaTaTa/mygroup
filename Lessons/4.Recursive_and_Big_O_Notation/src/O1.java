public class O1 {

    public static void main(String[] args) {
        //               0   1   2   3   4   5
        int[] numbers = {14, 16, 54, 78, 91, 34};


        //Мы берем элемент массива по индексу за три шага:
        //1. Берем адрес элемента под 0 индексом
        //2. Прибавляем к его адресу нужный нам индекс
        //3. Достаем нужный элемент
        System.out.println(getByIndex(numbers, 3));
    }

    //array[4]
    public static int getByIndex(int[] array, int index) {
        return array[index];
    }
}
