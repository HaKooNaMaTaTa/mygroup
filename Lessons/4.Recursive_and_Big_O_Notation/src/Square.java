public class Square {

    public static void main(String[] args) {
        int[] array = {14, 17, 21, 1, 2, 99}; //{1, 17, 21, 14, 2, 99}
        bubbleSort(array);

        int y = 0;
    }

    //{14, 17, 21, 1, 2, 99}
    //После первой итерации внешнего цикла
    //{1, 17, 21, 14, 2, 99}
    //После второй итерации внешнего цикла
    //{1, 14, 21, 17, 2, 99}
    //После третьей итерации внешнего цикла
    //{1, 14, 21, 17, 2, 99}
    //Сортировка Пузырьком
    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int x = 0;
            for (int j = i; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }
        }
    }
}
