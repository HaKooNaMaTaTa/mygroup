//Что бы понять рекурсию, нужно понять рекурсию.
public class Recursive {

    public static void main(String[] args) {
//        int number = factorialWithoutRecursive(5);
//        int number = factorialWithRecursive(5);
//        System.out.println(number);

        exampleSwitchCase("Привет");
    }

    //факториалБезРекурсии
    public static int factorialWithoutRecursive(int number) {
        int result = 1;

        //!5 = 1 * 2 * 3 * 4 * 5 = 120 -> !5 = 5 * !4 (4 * !3)
        for (int i = 1; i <= number; i++) {
            result = result * i;
        }

        return result;
    }

    //факториалСРекурсией
    public static int factorialWithRecursive(int number) {

        if (number == 1) {
            return 1;
        }
        return number * factorialWithRecursive(number - 1);
    }


    ///Пример switch-case
    public static void exampleSwitchCase(String text) {
//        if (text.equalsIgnoreCase("Привет")) {
//            System.out.println("И тебе пламенный салам!");
//        } else if (text.equalsIgnoreCase("Пока")) {
//            System.out.println("И тебе удачи!");
//        }

        //TODO: equalsIgnoreCase
        switch (text) {
            case "Привет":
                System.out.println("И тебе привет");
                break;
            case "Пока":
                System.out.println("И тебе покедова");
                break;
        }
    }
}
