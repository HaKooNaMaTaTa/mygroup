package com.maxima.mvc.service;

import com.maxima.mvc.dto.AccountDto;
import com.maxima.mvc.model.Account;

import java.util.List;

public interface AccountService {

    void saveAccount(AccountDto accountDto);

    List<Account> getAllAccounts();
}
