package com.maxima.mvc.repository;

import com.maxima.mvc.model.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository {

    void save(Account account);

    List<Account> getAllAccounts();
}
