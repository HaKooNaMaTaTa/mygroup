package com.maxima.mvc.controller;

import com.maxima.mvc.dto.AccountDto;
import com.maxima.mvc.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class SignUpController implements Controller {

    private AccountService accountService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        //POST, Post, post
        if (request.getMethod().equalsIgnoreCase("post")) {
            AccountDto dto = AccountDto.builder()
                    .firstName(request.getParameter("firstName"))
                    .lastName(request.getParameter("lastName"))
                    .email(request.getParameter("email"))
                    .password(request.getParameter("password"))
                    .build();
            accountService.saveAccount(dto);
            modelAndView.setViewName("accounts");
        } else {
            modelAndView.setViewName("signUp");
        }

        return modelAndView;
    }
}
