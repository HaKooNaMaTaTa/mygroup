package maxima.app;

import maxima.config.SpringConfig;
import maxima.models.Account;
import maxima.services.SignInService;
import maxima.services.SignUpService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainAnnotation {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        SignUpService signUpService = (SignUpService) applicationContext.getBean("signUpService");

        signUpService.save(Account.builder()
                .firstName("Oleg")
                .lastName("Igonin")
                .email("oleg@mail.com")
                .password("qwerty!@#45")
                .build());

        System.out.println("*******************");

        SignInService signInService = (SignInService) applicationContext.getBean("signInService");
        signInService.signIn();
    }
}
