package maxima.app;

import maxima.models.Account;
import maxima.repository.AccountRepository;
import maxima.repository.AccountRepositoryImpl;
import maxima.services.SignUpService;
import maxima.services.SignUpServiceImpl;
import maxima.validators.BlackListPasswords;
import maxima.validators.BlackListPasswordsImpl;
import maxima.validators.EmailValidatorByCharacter;
import maxima.validators.PasswordValidator;
import maxima.validators.PasswordValidatorByCharacter;

public class Main {
    public static void main(String[] args) {
        EmailValidatorByCharacter emailValidator = new EmailValidatorByCharacter();
        emailValidator.setPattern(".+@.+");
        PasswordValidator passwordValidator = new PasswordValidatorByCharacter();
        BlackListPasswords blackListPasswords = new BlackListPasswordsImpl();
        AccountRepository accountRepository = new AccountRepositoryImpl();

        SignUpService signUpService = new SignUpServiceImpl(
                emailValidator,
                passwordValidator,
                blackListPasswords,
                accountRepository);

        signUpService.save(Account.builder()
                .firstName("Oleg")
                .lastName("Igonin")
                .email("oleg@mail.com")
                .password("qwerty!@#45")
                .build());
    }
}
