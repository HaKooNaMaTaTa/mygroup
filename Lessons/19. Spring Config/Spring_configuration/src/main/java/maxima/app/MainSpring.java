package maxima.app;

import maxima.models.Account;
import maxima.services.SignUpService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainSpring {
    public static void main(String[] args) {
        //Контейнер бинов
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("context.xml");

        SignUpService signUpService = (SignUpService) applicationContext.getBean("signUpService");

        signUpService.save(Account.builder()
                .firstName("Oleg")
                .lastName("Igonin")
                .email("oleg@mail.com")
                .password("qwerty!@#45")
                .build());

        int i = 0;
    }
}
