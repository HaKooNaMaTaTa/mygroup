package maxima.repository;


import maxima.models.Account;
import org.springframework.stereotype.Repository;


@Repository
public class AccountRepositoryImpl implements AccountRepository {
    @Override
    public void save(Account account) {
        System.out.println("Аккаунт сохранен");
        System.out.println(account.getFirstName());
        System.out.println(account.getLastName());
        System.out.println(account.getEmail());
        System.out.println(account.getPassword());
    }
}
