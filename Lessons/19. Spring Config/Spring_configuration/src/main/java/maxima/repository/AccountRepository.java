package maxima.repository;


import maxima.models.Account;

public interface AccountRepository {

    void save(Account account);
}
