package maxima.config;

import maxima.services.SignInService;
import maxima.services.SignInServiceImpl;
import maxima.services.SignUpService;
import maxima.services.SignUpServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

//Данная аннотация говорит Spring`у, что данный класс содержит какие то настройки
//в него обязательно нужно заглянуть
@Configuration
@ComponentScan(basePackages = "maxima")
@PropertySource("classpath:application.properties")
public class SpringConfig {

    //@Bean vs @Component(@Service, @Controller, @Repository, @Configuration)
    @Bean("signInService")
    public SignInService getSignInService() {
        SignInServiceImpl signInService = new SignInServiceImpl();
        signInService.setConfigField("Это поле конфига. Оно очень важное. Мы его настроили.");
        return signInService;
    }
}
