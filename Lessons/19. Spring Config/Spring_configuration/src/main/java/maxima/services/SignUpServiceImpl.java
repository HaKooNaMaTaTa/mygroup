package maxima.services;

import maxima.models.Account;
import maxima.repository.AccountRepository;
import maxima.validators.BlackListPasswords;
import maxima.validators.EmailValidator;
import maxima.validators.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("signUpService") //signUpServiceImpl
public class SignUpServiceImpl implements SignUpService {

    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    private final BlackListPasswords blackListPasswords;
    private final AccountRepository accountRepository;

    @Autowired
    public SignUpServiceImpl(@Qualifier("emailByChar") EmailValidator emailValidator,
                             @Qualifier("pasByChar") PasswordValidator passwordValidator,
                             BlackListPasswords blackListPasswords,
                             AccountRepository accountRepository) {
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
        this.blackListPasswords = blackListPasswords;
        this.accountRepository = accountRepository;
    }

    @Override
    public void save(Account account) {
        blackListPasswords.contains(account.getPassword());
        emailValidator.validate(account.getEmail());
        passwordValidator.validate(account.getPassword());

        accountRepository.save(account);
    }
}
