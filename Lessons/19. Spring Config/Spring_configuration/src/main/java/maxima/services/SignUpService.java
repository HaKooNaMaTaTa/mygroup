package maxima.services;


import maxima.models.Account;

public interface SignUpService {

    void save(Account account);
}
