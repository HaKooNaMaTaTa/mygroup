package maxima.services;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
public class SignInServiceImpl implements SignInService {

    private String configField;

    @Override
    public void signIn() {
        System.out.println(configField);
    }
}
