package maxima.validators;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//Данная аннотация, говорит Spring`у, что объект данного класса
//должен создаваться самим Spring`ом
@Service // == <bean id="blackList" class="maxima.validators.BlackListPasswordsImpl"/>
public class BlackListPasswordsImpl implements BlackListPasswords {

    private final List<String> badPasswords = new ArrayList<>(Arrays.asList("admin", "qwerty", "12345"));
    @Override
    public boolean contains(String password) {
        if (badPasswords.contains(password)) {
            throw new IllegalArgumentException("Ваш пароль - уже слит в интернет. Придумайте другой");
        }

        return true;
    }
}
