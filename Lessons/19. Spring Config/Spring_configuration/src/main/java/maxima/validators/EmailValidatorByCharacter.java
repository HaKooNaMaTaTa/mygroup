package maxima.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service("emailByChar")
public class EmailValidatorByCharacter implements EmailValidator {

    //Объект данного класса принимает в себя, при создании, некий шаблон
    //и мы можем с помощью этого шаблона проверять строки
    //соответствуют ли они этому шаблону
    private Pattern pattern;

    //oleg@mail.com
    @Override
    public boolean validate(String email) {
        //Проверяет строку на соответствие шаблону
        //Если строка не соответствует, то выкидываем исключение
        //В нашем случае - почта введена без '@'
        if (!pattern.matcher(email).find()) {
            throw new IllegalArgumentException("Email введен некорректно. Проверьте email.");
        }

        return true;
    }

    @Autowired
    public void setPattern(@Value("${validator.email.regex}")String pattern) {
        this.pattern = Pattern.compile(pattern);
    }
}
