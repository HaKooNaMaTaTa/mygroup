package maxima.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("emailByLength")
@Primary //Из всех бинов, которые могут быть подставлены в переменную EmailValidator, данный класс в приоритете
public class EmailValidatorByLength implements EmailValidator {


    private int minLength;

    public EmailValidatorByLength(@Value("${validator.email.min-length}") int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean validate(String email) {
        if (email.length() < minLength) {
            throw new IllegalArgumentException("Email введен некорректно. Проверьте email.");
        }

        return true;
    }
}
