package maxima.validators;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Indexed TODO
@Service("pasByChar")
public class PasswordValidatorByCharacter implements PasswordValidator {
    @Override
    public boolean validate(String password) {
        if (password.indexOf('!') == -1 || password.indexOf('@') == -1 || password.indexOf('#') == -1) {
            throw new IllegalArgumentException("Пароль не содержит спецсимволы - !, @, #");
        }

        return true;
    }
}
