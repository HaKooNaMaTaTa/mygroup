package maxima.validators;

public interface EmailValidator {

    boolean validate(String email);
}
