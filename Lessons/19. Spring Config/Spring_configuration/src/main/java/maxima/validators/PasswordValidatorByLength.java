package maxima.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//PasswordValidatorByLength - passwordValidatorByLength
@Service("pasByLength")
@Primary
public class PasswordValidatorByLength implements PasswordValidator {


    private int minLength;

    public PasswordValidatorByLength(@Value("${validator.password.min-length}") int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean validate(String password) {
        if (password.length() < minLength) {
            throw new IllegalArgumentException("Пароль слишком короткий");
        }
        return true;
    }
}
