package com.maxima.security_architecture.config.user_details;

import com.maxima.security_architecture.model.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class AccountUserDetails implements UserDetails {

	private final Account account;

	public AccountUserDetails(Account account) {
		this.account = account;
	}

	//Какие права есть у нашего пользователя
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.singleton(new SimpleGrantedAuthority(account.getRole().name()));
	}

	//Как взять пароль у нашего аккаунта?
	@Override
	public String getPassword() {
		return account.getPassword();
	}
	//Какой username у нашего аккаунта?
	@Override
	public String getUsername() {
		return account.getEmail();
	}

	//Аккаунт не просрочен?
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	//Аккаунт не заблокирован?
	@Override
	public boolean isAccountNonLocked() {
		return !account.getState().equals(Account.State.BANNED);
	}

	//Не просрочены ли наши креды - логин и пароль
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	//Активен ли наш аккаунт?
	@Override
	public boolean isEnabled() {
		return account.getState().equals(Account.State.CONFIRMED);
	}
}
