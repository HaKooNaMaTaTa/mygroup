package com.maxima.security_architecture.controller;

import com.maxima.security_architecture.dto.AccountDto;
import com.maxima.security_architecture.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/signUp") //указали на какой url будет реагировать данный контроллер
public class SignUpController {

    private final AccountService accountService;

    @Autowired
    public SignUpController(AccountService accountService) {
        this.accountService = accountService;
    }

    //    @GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String getSignUpPage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/profile";
        }
        return "signUp";
    }

    //@PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String signUp(AccountDto dto) {
        accountService.saveAccount(dto);
        return "redirect:/signIn";
    }
}
