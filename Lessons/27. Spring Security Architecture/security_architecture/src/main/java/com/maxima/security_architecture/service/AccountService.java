package com.maxima.security_architecture.service;

import com.maxima.security_architecture.dto.AccountDto;
import com.maxima.security_architecture.dto.SignInForm;
import com.maxima.security_architecture.model.Account;

import java.util.List;

public interface AccountService {

    void saveAccount(AccountDto accountDto);

    List<Account> getAllAccounts();

    Boolean signIn(SignInForm form);

    Account getById(Long id);

    void delete(Long id);
}
