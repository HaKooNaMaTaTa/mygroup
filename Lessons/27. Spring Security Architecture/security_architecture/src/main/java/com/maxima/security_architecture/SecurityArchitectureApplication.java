package com.maxima.security_architecture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityArchitectureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityArchitectureApplication.class, args);
	}

}
