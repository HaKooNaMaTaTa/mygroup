package com.maxima.security_architecture.controller;

import com.maxima.security_architecture.dto.SignInForm;
import com.maxima.security_architecture.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/signIn") //указали на какой url будет реагировать данный контроллер
@RequiredArgsConstructor
public class SignInController {

    private final AccountService accountService;

//    @GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String getSignInPage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/profile";
        }
        return "signIn";
    }

//    @PostMapping
    @RequestMapping(method = RequestMethod.POST)
    public String signIn(SignInForm form) {
        if (accountService.signIn(form)) {
            return "redirect:/profile";
        } else {
            return "redirect:/signIn?error";
        }
    }
}
