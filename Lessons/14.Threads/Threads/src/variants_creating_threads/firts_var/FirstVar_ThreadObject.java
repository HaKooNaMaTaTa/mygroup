package variants_creating_threads.firts_var;

public class FirstVar_ThreadObject {
    public static void main(String[] args) {
        Thread thread1 = new Thread("Thread number one");
        Thread thread2 = new Thread("Thread number two");
        Thread thread3 = new Thread("Thread number three");

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
