package variants_creating_threads.second_var;

public class Main {

    public static void main(String[] args) {
        SecondVar_ExtendsThread thread1 = new SecondVar_ExtendsThread("Oleg");
        SecondVar_ExtendsThread thread2 = new SecondVar_ExtendsThread("Gladiolus");
        SecondVar_ExtendsThread thread3 = new SecondVar_ExtendsThread("Muhtar");

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
