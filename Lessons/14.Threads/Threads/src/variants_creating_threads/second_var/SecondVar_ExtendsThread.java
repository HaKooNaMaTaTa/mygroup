package variants_creating_threads.second_var;

public class SecondVar_ExtendsThread extends Thread {


    public SecondVar_ExtendsThread(String name) {
        super(name);
    }

    //ВСЕГДА переопределяем метод run(), а не start()
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Запущен поток под именем - " + getName());
        }
    }
}
