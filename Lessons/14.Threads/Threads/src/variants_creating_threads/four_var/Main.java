package variants_creating_threads.four_var;

public class Main {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            int i = 0;

            while (i <= 100) {
                System.out.println(i++);
            }
        });

        thread.start();
    }
}
