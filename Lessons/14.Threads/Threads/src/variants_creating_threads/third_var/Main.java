package variants_creating_threads.third_var;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new ThirdVar_ImplRunnable("Interface Runnable"));

        thread.start();

        System.out.println("Последняя строка класса Main");
    }
}
