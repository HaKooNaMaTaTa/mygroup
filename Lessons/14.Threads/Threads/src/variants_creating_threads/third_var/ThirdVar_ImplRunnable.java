package variants_creating_threads.third_var;

public class ThirdVar_ImplRunnable implements Runnable {

    private String name;

    public ThirdVar_ImplRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        for (int i = 0; i < 100; i++) {
            System.out.println("Запущен поток под именем - " + getName());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
