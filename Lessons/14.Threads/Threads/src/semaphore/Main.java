package semaphore;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3);

        new Car("А", semaphore);
        new Car("B", semaphore);
        new Car("C", semaphore);
        new Car("D", semaphore);
        new Car("E", semaphore);
        new Car("F", semaphore);
    }
}
