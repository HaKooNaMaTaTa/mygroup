package join;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Worker kopatel = new Worker("Копатель");
        Worker taskatel = new Worker("Таскатель");
        Worker zakapivatel = new Worker("Закапыватель");
        System.out.println("Ребят. Нужно выкопать здесь яму, и засыпать вооооон ту!");
//        kopatel.setPriority(7);
//        taskatel.setPriority(6);
//        zakapivatel.setPriority(5);
        kopatel.start();
        //Поток, в котором вызвали .join(), ждет заверешения работы потока, у которого вызвали этот метод
        kopatel.join(); //main ждет, когда закончит работу поток Копатель
        taskatel.start();
        taskatel.join();
        //Поток демон - вспомогательный поток, процесс не будет ждать его завершения. Если основные потоки закончили работу
        //то, процесс завершается, даже если потоки демоны не закончили свою работу
        zakapivatel.setDaemon(true);
        zakapivatel.start();
        System.out.println("Я уехал, завтра проверю");
    }
}
