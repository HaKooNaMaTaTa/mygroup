package credit_card;

public class Main {
    public static void main(String[] args) {
        CreditCard creditCard = new CreditCard(1000);
        Person wife = new Person("Жена", creditCard);
        Person husband = new Person("Муж", creditCard);

        wife.start();
        husband.start();
    }
}
