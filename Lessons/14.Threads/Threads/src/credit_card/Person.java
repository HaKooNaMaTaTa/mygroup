package credit_card;

public class Person extends Thread {

    private CreditCard creditCard;

    public Person(String name, CreditCard creditCard) {
        super(name);
        this.creditCard = creditCard;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            //построили телефонную будку с помощью мьютекса кредитной карты,
            //т.к. мьютекс есть абсолютно у всех объектов.
            synchronized (creditCard) {
                System.out.println(getName() + " идет совершать покупки!");
                if (creditCard.pay(10)) {
                    System.out.println(getName() + " совершил покупку");
                } else {
                    System.out.println(getName() + " - Лебовски, где деньги?");
                }
                System.out.println(getName() + " пришел домой!");
            }
        }
    }
}
