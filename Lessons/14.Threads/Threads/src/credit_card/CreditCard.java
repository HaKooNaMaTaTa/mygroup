package credit_card;

public class CreditCard {

    private int balance;

    public CreditCard(int balance) {
        this.balance = balance;
    }

    public boolean pay(int cost) {
        if (cost <= balance) {
            this.balance -= cost;
            System.out.println("Successful! Balance: " + balance);
            return true;
        }

        System.out.println("Failed!");
        return false;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
