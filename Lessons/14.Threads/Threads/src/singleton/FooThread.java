package singleton;

public class FooThread extends Thread {

    @Override
    public void run() {
        MySingleton mySingleton = MySingleton.getInstance("FOO");
        System.out.println(mySingleton.getMessage());
    }
}
