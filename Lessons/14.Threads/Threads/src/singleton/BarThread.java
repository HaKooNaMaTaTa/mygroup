package singleton;

public class BarThread extends Thread {

    @Override
    public void run() {
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        MySingleton mySingleton = MySingleton.getInstance("BAR");
        System.out.println(mySingleton.getMessage());
    }
}
