package singleton;

public class Main {

    public static void main(String[] args) {
        //Однопоточность
//        MySingleton singleton1 = MySingleton.getInstance("FOO");
//        MySingleton singleton2 = MySingleton.getInstance("BAR");
//
//        System.out.println(singleton1.getMessage());
//        System.out.println(singleton2.getMessage());

        //Многопоточность
        BarThread barThread = new BarThread();
        FooThread fooThread = new FooThread();

        barThread.start();
        fooThread.start();
    }
}
