package singleton;

public class MySingleton {
    //volatile - отключает кэш у переменных.
    private volatile static MySingleton instance;
    private String message;

    private MySingleton(String message) {
        this.message = message;
    }

    //synchronized - делает так, что в данном методе, одновременно, может находиться только один поток
    public synchronized static MySingleton getInstance(String message) {
        if (instance == null) {
            instance = new MySingleton(message);
        }

        return instance;
    }

    public String getMessage() {
        return message;
    }
}
