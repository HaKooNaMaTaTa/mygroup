package oop;

public class Car {
    //Переменные класса (Поля)
    String brand;
    String model;

    //Конструктор - набор инструкций по инциализации объекта.
    //Наименование конструктора всегда совпадает с наименованием класса
    //Пустой конструктор
    public Car() {}

    //Конструктор по умолчанию - создается компилятором, если он не находит конструктор в классе
//    public oop.Car() { <- создается компилятором
//    }

    //Конструктор с параметрами - набор инструкций по инициализации объекта конкретными значениями
//    public oop.Car(String brand, String model) {
//        //this. - это ссылка на сам объект данного класса
//        this.brand = brand;
//        this.model = model;
//    }


    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    public Car(String brand) {
        this.brand = brand;
    }
}
