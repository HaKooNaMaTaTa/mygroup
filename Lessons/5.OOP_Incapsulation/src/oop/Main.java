package oop;

public class Main {
    public static void main(String[] args) {
        //TODO: сколько памяти занимает CAR?
        //ТИП_ДАННЫХ НАИМЕНОВАНИЕ_ПЕРЕМЕННОЙ = new (ОПЕРАТОР_ВЫДЕЛЕНИЯ_ПАМЯТИ) КОНСТРУКТОР()
        Car mercedes = new Car("Oleg");
        Car bmw = new Car("BMW", "X5");
//        bmw.brand = "BMW";
//        bmw.model = "X5";
        Car audi = new Car("Audi", "A6");

        System.out.println("Мерседес");
        System.out.println("Поле brand - " + mercedes.brand);
        System.out.println("Поле model - " + mercedes.model);

        System.out.println("БМВ");
        System.out.println("Поле brand - " + bmw.brand);
        System.out.println("Поле model - " + bmw.model);

        System.out.println("Ауди");
        System.out.println("Поле brand - " + audi.brand);
        System.out.println("Поле model - " + audi.model);


    }
}
