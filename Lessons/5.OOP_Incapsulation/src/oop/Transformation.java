package oop;

public class Transformation {

    public static void main(String[] args) {
        //double - 8 байт
        //int - 4 байта
        //Явное преобразование - преобразование одного типа данных в другой, с потерей данных
        System.out.println("Из double в int");
        double a = Math.random() * 1000;
        int b = (int) a;

        System.out.println(a);
        System.out.println(b);

        System.out.println("Из int в double");
        int c = 12456;
        double d = c;

        System.out.println(c);
        System.out.println(d);

        System.out.println("Из byte в int");
        byte e = 124;
        int f = e;

        System.out.println(e);
        System.out.println(f);

        System.out.println("Из int в byte");
        //TODO: Почитать про переполнение
        int g = 1240;
        byte h = (byte) g;

        System.out.println(g);
        System.out.println(h);
    }
}
