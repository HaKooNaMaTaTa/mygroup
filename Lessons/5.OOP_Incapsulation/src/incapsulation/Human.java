package incapsulation;

/* 4 типа модификаторов доступа:
 * 1. public - данный класс, метод, переменная и конструктор - доступны во всей программе
 * 2. private - данный метод, переменная и конструктор - доступны только внутри класса
 * 3. protected
 * 4. default package - данный класс, метод, переменная и конструктор - доступны внутри папки, которой они находятся
 */
public class Human {

    private String firstName;
    private String lastName;
    private int age;

    public Human(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    private void sayHello() {
        System.out.println(firstName + " " + lastName + " говорит Вам привет! Мне - " + age + " годиков");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
