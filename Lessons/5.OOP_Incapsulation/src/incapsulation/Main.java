package incapsulation;

public class Main {

    public static void main(String[] args) {

        Human human = new Human("Oleg", "Igonin", 27);

        human.setFirstName("Igor");
        human.setAge(-12);
        human.sayHello();

        System.out.println(human.getFirstName());
        System.out.println(human.getLastName());
    }
}
