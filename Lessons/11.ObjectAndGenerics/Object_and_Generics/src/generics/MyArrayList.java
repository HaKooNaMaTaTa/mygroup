package generics;

import objects.Car;

//Дженерик - обобщенный тип. Отображается с помощью diamonds(<>)
//T - type
//E - element
//K - key
public class MyArrayList<T extends Car> {

    private final int DEFAULT_SIZE = 10;
    private Object[] array;
    private int count = 0;

    public MyArrayList(T[] array) {
        this.array = array;
    }

    public MyArrayList() {
        this.array = new Object[DEFAULT_SIZE];
    }

    public MyArrayList(int size) {
        this.array = new Object[size];
    }

    public boolean add(T element) {
        if (count < array.length) {
            array[count] = element;
            count++;
            return true;
        }
        return false;
    }

    public boolean set(T element, int index) {
        if (index < array.length) {
            array[index] = element;
            return true;
        }

        return false;
    }
}
