package generics;

public class Main {
    public static void main(String[] args) {
        IntegerArray integerArray = new IntegerArray();
        int count = 10;
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.add(count++);
        integerArray.set(0, 0);
        integerArray.add(count++);
    }
}
