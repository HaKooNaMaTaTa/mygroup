package generics;

public class IntegerArray {

    private final int DEFAULT_SIZE = 10;
    private int[] array;
    private int count = 0;

    public IntegerArray(int[] array) {
        this.array = array;
    }

    public IntegerArray(int size) {
        this.array = new int[size];
    }

    public IntegerArray() {
        this.array = new int[DEFAULT_SIZE];
    }

    public boolean add(int element) {
        if (count < array.length) {
            array[count] = element;
            count++;
            return true;
        }
        return false;
    }

    //TODO: Реализовать
    public boolean add(int element, int index) {
        return false;
    }

    public boolean set(int element, int index) {
        if (index < array.length) {
            array[index] = element;
            return true;
        }
        return false;
    }
}
