package generics;

import objects.Car;

//Для каждого примитива есть класс обертка с аналогичным наименованием, т.е. int - Integer, byte - Byte
public class MainGeneric {
    public static void main(String[] args) {
//        MyArrayList<Integer> integers = new MyArrayList<>();
//
//        integers.add(10);
//        integers.add(10);
//        integers.add(10);
//        integers.add(10);
//        integers.add(10);
//        integers.add(10);
//        integers.add(10);

        //если не указать с каким типом данных мы работаем, то по умолчанию будет подставлен Object
        //MyArrayList<Object> cars
        MyArrayList<Car> cars = new MyArrayList();

        cars.add(new Car("Brand", "Model", "Color"));
        cars.add(new Car("Brand", "Model", "Color"));
        cars.add(new Car("Brand", "Model", "Color"));
        cars.add(new Car("Brand", "Model", "Color"));
        cars.add(new Car("Brand", "Model", "Color"));
        cars.add(new Car("Brand", "Model", "Color"));
        cars.add(new Car("Brand", "Model", "Color"));

    }
}
