package objects;

public class HashCodeEx {
    public static void main(String[] args) {
        Car bmw = new Car("BMW", "X5", "Black");
        Car audi = new Car("Audi", "A6", "White");
        Car anotherBmw = new Car("BMW", "X5", "Black");

//        System.out.println(bmw.equals(audi));
//        System.out.println("Hashcode BMW - " + bmw.hashCode());
//        System.out.println("Hashcode AUDI - " + audi.hashCode());

        System.out.println(bmw.equals(anotherBmw));
        System.out.println("Hashcode BMW - " + bmw.hashCode());
        System.out.println("Hashcode another BMW - " + anotherBmw.hashCode());

        System.out.println(Integer.MAX_VALUE + 1);
    }
}
