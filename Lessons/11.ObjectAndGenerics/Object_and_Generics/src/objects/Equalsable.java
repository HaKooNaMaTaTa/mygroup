package objects;

public interface Equalsable {
    boolean equals(Object o);
}
