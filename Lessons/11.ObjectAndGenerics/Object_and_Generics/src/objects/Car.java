package objects;

import java.util.Objects;
import java.util.Random;

public class Car {

    private String brand;
    private String model;
    private String color;

    public Car(String brand, String model, String color) {
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return brand + "|" + model;
    }

    //equals(переопределенный)
    @Override
    public boolean equals(Object o) {
        //Если переданный объект это сам объект, у которого мы вызываем
        if (this == o) return true;
        //Если передается пустота или же сравниваемые объекты от разных классов
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return brand.equals(car.brand) && model.equals(car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model);
    }
}
