package objects;

public class Main {

    public static void main(String[] args) {
        Object obj = new Car("Brand", "Model", "Black");
        Car car = new Car("Bmw", "X5", "Black");
        Car car1 = new Car("Bmw", "X5", "White");
        Car car2 = new Car("Bmw", "X5", "Red");
        SportCar sportCar = new SportCar("Ferrari", "Enzo", "Red");
        System.out.println(car instanceof Object);

        Class classCar = car.getClass();

        int i = 0;

        System.out.println(car);
        System.out.println(car1);
        System.out.println(car2);

        System.out.println(car.hashCode());
        System.out.println(car1.hashCode());
        System.out.println(car2.hashCode());

        car1.equals(car1);

        System.out.println(car.equals(car1));
        System.out.println(car1.equals(car));
        System.out.println(sportCar.equals(car));

        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car.setBrand("");
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);
        car1.equals(car);

        Car car3 = null;

      boolean result = null == null;

    }
}
