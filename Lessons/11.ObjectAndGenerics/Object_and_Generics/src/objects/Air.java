package objects;

public class Air implements Equalsable {

    private String name;

    public Air(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

