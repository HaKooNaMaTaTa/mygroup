package objects;

public class TestInterfaces {
    public static void main(String[] args) {
        Air air = new Air("Name");
        Air air2 = new Air("Name");

        System.out.println(air.equals(air2));
    }
}
