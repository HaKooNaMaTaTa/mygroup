package com.maxima.central_bank.repositories;

import com.maxima.central_bank.models.LoanDecision;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanDecisionRepository extends JpaRepository<LoanDecision, Long> {
}
