package com.maxima.central_bank.service;

import com.maxima.central_bank.models.LoanDecision;
import com.maxima.central_bank.repositories.LoanDecisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@EnableScheduling
public class LoanDecisionSchedulerService {

	private final LoanDecisionRepository loanDecisionRepository;

	@Autowired
	public LoanDecisionSchedulerService(LoanDecisionRepository loanDecisionRepository) {
		this.loanDecisionRepository = loanDecisionRepository;
	}

	@Scheduled(cron = "*/60 * * * * *")
	private void calculateLoanDecision() {
		List<LoanDecision> loanDecisions = loanDecisionRepository.findAll();
		loanDecisions.stream()
				.filter(loanDecision -> loanDecision.getFinalDecision() == null)
				.peek(loanDecision -> {
					Boolean choice = loanDecision.getRequestOnCredit().getClient().getGoodCreditHistory() &&
							(loanDecision.getRequestOnCredit().getClient().getCurrentMonthlySalary() -
									loanDecision.getRequestOnCredit().getClient().getCurrentMonthlyPayment()) > 30000;
					if (choice) {
						loanDecision.setFinalDecision(true);
						int amount = (int) (Math.random() * 1_000_000);
						loanDecision.setAmountOfCredit(amount);
						loanDecision.setMonthlyPayment(amount / 10);
					}
				})
				.forEach(loanDecisionRepository::save);
	}
}
