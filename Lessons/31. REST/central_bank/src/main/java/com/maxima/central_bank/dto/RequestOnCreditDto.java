package com.maxima.central_bank.dto;

import com.maxima.central_bank.models.ClientInfo;
import com.maxima.central_bank.models.LoanDecision;
import com.maxima.central_bank.models.RequestOnCredit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Информация по заявке на кредит

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestOnCreditDto {

	private Long id;

	private ClientInfo client;

	private LoanDecision loanDecision;

	private Integer amountOfCredit;
	private Integer monthlyPayment;

	public static RequestOnCreditDto from(RequestOnCredit requestOnCredit) {
		return RequestOnCreditDto.builder()
				.id(requestOnCredit.getId())
				.client(requestOnCredit.getClient())
				.loanDecision(requestOnCredit.getLoanDecision())
				.build();
	}
}
