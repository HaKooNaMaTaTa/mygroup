package com.maxima.central_bank.repositories;

import com.maxima.central_bank.models.RequestOnCredit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RequestOnCreditRepository extends JpaRepository<RequestOnCredit, Long> {

	Optional<RequestOnCredit> findByClient_Id(Long clientId);
}
