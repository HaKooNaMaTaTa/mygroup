package com.maxima.central_bank.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

//Решение банка по заявке на кредит

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoanDecision {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne
	@JsonIgnore
	private RequestOnCredit requestOnCredit;

	private Boolean finalDecision;
	private Integer amountOfCredit;
	private Integer monthlyPayment;
}
