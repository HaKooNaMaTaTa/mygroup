package com.maxima.central_bank.repositories;

import com.maxima.central_bank.models.ClientInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientInfoRepository extends JpaRepository<ClientInfo, Long> {
}
