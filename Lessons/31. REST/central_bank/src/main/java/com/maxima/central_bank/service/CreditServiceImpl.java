package com.maxima.central_bank.service;

import com.maxima.central_bank.dto.ClientInfoDto;
import com.maxima.central_bank.dto.RequestOnCreditDto;
import com.maxima.central_bank.models.ClientInfo;
import com.maxima.central_bank.models.LoanDecision;
import com.maxima.central_bank.models.RequestOnCredit;
import com.maxima.central_bank.repositories.ClientInfoRepository;
import com.maxima.central_bank.repositories.LoanDecisionRepository;
import com.maxima.central_bank.repositories.RequestOnCreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditServiceImpl implements CreditService {

	private final ClientInfoRepository clientInfoRepository;
	private final LoanDecisionRepository loanDecisionRepository;
	private final RequestOnCreditRepository requestOnCreditRepository;

	@Autowired
	public CreditServiceImpl(ClientInfoRepository clientInfoRepository,
							 LoanDecisionRepository loanDecisionRepository,
							 RequestOnCreditRepository requestOnCreditRepository) {
		this.clientInfoRepository = clientInfoRepository;
		this.loanDecisionRepository = loanDecisionRepository;
		this.requestOnCreditRepository = requestOnCreditRepository;
	}

	/**
	 * В первую очередь, нам нужно выяснить, существует ли данный пользователь уже в БД
	 * Если существует, то, возможно, у него уже есть заявка, значит нужно ее вернуть
	 * Если заявки нет, то создаем новую.
	 * Если пользователя не существует, то сохраняем его в БД и создаем заявку
	 * @param dto
	 * @return
	 */
	@Override
	public ResponseEntity<RequestOnCreditDto> createRequest(ClientInfoDto dto) {
		ClientInfo clientInfo;

		if (dto.getId() == null) {
			clientInfo = ClientInfo.builder()
					.firstName(dto.getFirstName())
					.lastName(dto.getLastName())
					.patronymic(dto.getPatronymic())
					.seriesPassport(dto.getSeriesPassport())
					.numberPassport(dto.getNumberPassport())
					.currentMonthlySalary(dto.getCurrentMonthlySalary())
					.currentMonthlyPayment(dto.getCurrentMonthlyPayment())
					.goodCreditHistory(dto.getGoodCreditHistory())
					.build();

			clientInfo = clientInfoRepository.save(clientInfo);
		} else {
			Optional<RequestOnCredit> requestOnCreditOptional = requestOnCreditRepository.findByClient_Id(dto.getId());
			if (requestOnCreditOptional.isPresent()) {
				return ResponseEntity.ok(RequestOnCreditDto.from(requestOnCreditOptional.get()));
			}
			//TODO: переделать на Optional
			//Доп задача - проверять поля на актуальность (т.е. в dto более новые значения)
			clientInfo = clientInfoRepository.getById(dto.getId());
		}

		// TODO: сумма и ежемесячный платеж были перенесены в RequestOnCredit
		RequestOnCredit requestOnCredit = new RequestOnCredit();
		requestOnCredit.setClient(clientInfo);
		requestOnCredit = requestOnCreditRepository.save(requestOnCredit);
		LoanDecision loanDecision = new LoanDecision();
		loanDecision.setRequestOnCredit(requestOnCredit);
		loanDecision = loanDecisionRepository.save(loanDecision);
		requestOnCredit.setLoanDecision(loanDecision);
		requestOnCredit = requestOnCreditRepository.save(requestOnCredit);

		return ResponseEntity.ok(RequestOnCreditDto.from(requestOnCredit));
	}
}
