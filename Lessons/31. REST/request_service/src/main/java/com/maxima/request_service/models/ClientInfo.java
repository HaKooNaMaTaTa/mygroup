package com.maxima.request_service.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String firstName;
	private String lastName;
	private String patronymic;
	private String seriesPassport;
	private String numberPassport;
	private Integer currentMonthlyPayment;
	private Integer currentMonthlySalary;
	//TODO: на уроке будем проставлять это ручками,
	// но нужно переписать на автоматическое вычисление данного параметра
	// парметр вычисляется в central_bank
	private Boolean goodCreditHistory;
}
