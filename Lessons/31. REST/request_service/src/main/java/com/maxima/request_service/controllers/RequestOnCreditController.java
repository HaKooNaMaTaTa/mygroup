package com.maxima.request_service.controllers;

import com.maxima.request_service.dto.ClientInfoDto;
import com.maxima.request_service.dto.RequestOnCreditDto;
import com.maxima.request_service.services.RequestOnCreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//в качестве конечного ресурса никогда не надо
//использовать слова глаголы
@RequestMapping("/request-on-credit")
public class RequestOnCreditController {

	private final RequestOnCreditService requestOnCreditService;

	@Autowired
	public RequestOnCreditController(RequestOnCreditService requestOnCreditService) {
		this.requestOnCreditService = requestOnCreditService;
	}

	@PostMapping
	public ResponseEntity<RequestOnCreditDto> createRequest(@RequestBody ClientInfoDto dto) {
		return requestOnCreditService.createRequest(dto);
	}
}
