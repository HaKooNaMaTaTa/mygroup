package com.maxima.request_service.services;

import com.maxima.request_service.dto.ClientInfoDto;
import com.maxima.request_service.dto.RequestOnCreditDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RequestOnCreditServiceImpl implements RequestOnCreditService {

	private final RestTemplate restTemplate = new RestTemplate();
	@Override
	public ResponseEntity<RequestOnCreditDto> createRequest(ClientInfoDto clientInfo) {
		//TODO: адрес вынести в какой нибудь общий либо файл, либо класс
		// TODO: добавить проверку полей, что бы все поля, кроме кредитной истории - были проставлены
		return restTemplate.postForEntity("http://localhost:8080/credit", clientInfo, RequestOnCreditDto.class);
	}
}
