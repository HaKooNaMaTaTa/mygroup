package com.maxima.request_service.services;

import com.maxima.request_service.dto.ClientInfoDto;
import com.maxima.request_service.dto.RequestOnCreditDto;
import org.springframework.http.ResponseEntity;

public interface RequestOnCreditService {
	//Это обертка, внутри которой можно настроить, к примеру:
	//Статус ответа, заголовки, внутрь поместить сам результат вычислений
	ResponseEntity<RequestOnCreditDto> createRequest(ClientInfoDto clientInfo );
}
