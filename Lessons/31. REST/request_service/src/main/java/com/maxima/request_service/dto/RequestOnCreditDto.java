package com.maxima.request_service.dto;

import com.maxima.request_service.models.ClientInfo;
import com.maxima.request_service.models.LoanDecision;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Информация по заявке на кредит

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestOnCreditDto {

	private Long id;

	private ClientInfo client;

	private LoanDecision loanDecision;
}
