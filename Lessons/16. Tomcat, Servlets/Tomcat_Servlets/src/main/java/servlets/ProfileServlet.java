package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile/*")
public class ProfileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Вытаскиваешь из URL id пользователя
        //Находишь пользователя по этому id
        //закидывыешь данные пользователя в запрос (req.setAttribute())
        //Перенаправляешь запрос между сервлетами
        String stroke = req.getRequestURI();
        req.getRequestDispatcher("jsp/signUp.jsp").forward(req, resp);
    }
}
