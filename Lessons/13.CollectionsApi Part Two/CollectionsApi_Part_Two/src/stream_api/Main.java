package stream_api;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();

        int count = 15;
        Random random = new Random();
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
//        integers.add(count++);
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));
        integers.add(random.nextInt(100));

//        for (int i = 0; i < integers.size(); i++) {
//            if (integers.get(i) % 2 == 0) {
//                System.out.println(integers.get(i));
//            }
//        }
        /*
        В Stream API есть два типа методов:
            1. Конвейерный тип метода - до работы метода был поток данных и после работы метода вышел поток данных
            2. Терминальный тип метода - до работы метода был поток данных, а после работы метода мы получили что то конкретное (коллекцию, объект, или какое то действие)
         */
        integers.stream()
                .sorted()
                .map(num -> {
                    if (num % 2 != 0) {
                        throw new ArithmeticException();
                    } else {
                        return String.valueOf(num);
                    }
                })
                .filter(num -> Integer.parseInt(num) % 2 == 0)
                .forEach(System.out::println); //:: (квадроточие) - ссылка на метод
    }
}
