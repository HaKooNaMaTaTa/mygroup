package map;

import map.example.Command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();

        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);
        integers.add(10);

        //Сначала идет ТИП_ДАННЫХ_КЛЮЧА, ТИП_ДАННЫХ_ЗНАЧЕНИЯ
        Map<String, Integer> map = new HashMap<>();

        map.put("Десять", 10);
        System.out.println(map.containsKey("Десять"));
        System.out.println(map.containsKey("Двадцать один"));
        Map<String, String> params = new HashMap<>();

        params.put("age", "25");
        String q = "SELECT * FROM HUMANS WHERE age = #{params.age}, name = null";



//        Map<String, Command> commandMap = new HashMap<>();
//
//        commandMap.put("Запись на консультацию", new Command("Запись на консультацию"));
//        commandMap.put("Запись на прием", new Command("Запись на прием"));
//        commandMap.put("Запись на урок", new Command("Запись на урок"));

    }
}
