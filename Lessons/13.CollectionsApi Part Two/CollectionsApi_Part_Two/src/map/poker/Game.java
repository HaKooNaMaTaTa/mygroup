package map.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Card[] cards = new Card[5];

        System.out.println("Введите карты");
        for (int i = 0; i < cards.length; i++) {
            cards[i] = new Card(scanner.next());
        }

        System.out.println(Arrays.toString(cards));

        Map<Card, Integer> map = new HashMap<>();
        //K K K 10 10
        //K 3, 10 2
        for (int i = 0; i < cards.length; i++) {
            if (map.containsKey(cards[i])) {
                int temp = map.get(cards[i]);
                temp += 1; //temp++
                map.put(cards[i], temp);
                //map.put(card[i], map.get(card[i])++)
            } else {
                map.put(cards[i], 1);
            }
        }

        //K K K K K
        //K 5
        //1 var:
        //K K K J J
        //2 var
        //K K K K J
        //1var
        //K K J J 10
        //2var
        //K K K J 10
        switch (map.size()) {
            case 1:
                System.out.println("ЖУУУУУЛИК!!!");
                break;
            case 2:
                if (checkValues(map, 4)) {
                    System.out.println("Поздравляю, КАРРРРЭ!");
                } else {
                    System.out.println("Поздравляю, фул хаус");
                }
                break;
            case 3:
                if (checkValues(map, 3)) {
                    System.out.println("СЭЭЭЭЭТ");
                } else {
                    System.out.println("ДВЕЕЕЕ ПААААРЫЫ");
                }
                break;
            case 4:
                System.out.println("ПААААРАА");
                break;
            case 5:
                System.out.println("Looooshaaaara");
        }
    }

    public static boolean checkValues(Map<Card, Integer> map, int checkValue) {
        //Взяли и выгрузили в список list все значения, которые есть в нашей мапе
        List<Integer> list = new ArrayList<>(map.values());

        for (Integer integer : list) {
            if (integer == checkValue) {
                return true;
            }
        }

        return false;
    }
}
