package map.example;

public class RegistrationCommand extends Command {

    public RegistrationCommand(String nameCommand) {
        super(nameCommand);
    }

    @Override
    public void runCommand() {
        System.out.println("Вы хотите зарегистрироваться!");
        System.out.println("Круто, удачи!");
    }
}
