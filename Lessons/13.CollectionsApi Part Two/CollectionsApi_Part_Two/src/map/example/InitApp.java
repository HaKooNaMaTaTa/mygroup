package map.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InitApp {

    public static void main(String[] args) {
        Map<String, Command> mapCommand = new HashMap<>();

        //Момент поднятия приложения.
        mapCommand.put("Запись", new RecordCommand("Консультация 24"));
        mapCommand.put("Регистрация", new RegistrationCommand("Welcome Регистрация"));

        Scanner scanner = new Scanner(System.in);
        String stroke = scanner.nextLine();
        switch (stroke) {
            case "Запись":
                mapCommand.get("Запись").runCommand();
                break;
            case "Регистрация":
                mapCommand.get("Регистрация").runCommand();
                break;
            default:
                System.out.println("То, что вы попросили - мы не умеем");
        }
    }
}
