package map.example;

public class RecordCommand extends Command {


    public RecordCommand(String nameCommand) {
        super(nameCommand);
    }

    @Override
    public void runCommand() {
        System.out.println("Вы хотите записаться на консультацию!");
        System.out.println("Это очень здорово, спасибо, что выбрали нас!");
        System.out.println("Все в отпуске, спасибо!");
    }
}
