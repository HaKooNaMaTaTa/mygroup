package map.example;
// команда для телеграм бота
public abstract class Command {

    private String nameCommand;

    public Command(String nameCommand) {
        this.nameCommand = nameCommand;
    }

    public abstract void runCommand();

    public String getNameCommand() {
        return nameCommand;
    }

    public void setNameCommand(String nameCommand) {
        this.nameCommand = nameCommand;
    }
}
