package set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetMain {
    public static void main(String[] args) {
        Set<Integer> integers = new HashSet<>();

        System.out.println(integers.add(10));
        System.out.println(integers.add(10));
        System.out.println(integers.add(10));
        System.out.println(integers.add(10));

        for (Integer integer : integers) {
            System.out.println(integer);
        }
    }
}
