package pattern_dao;

public class Car {
    //BMW X5 | new Car("BMW", "X5")
    private static int COUNT_ID = 0;
    private int id;
    private String brand;
    private String model;

    public Car(String brand, String model) {
        this.id = COUNT_ID;
        this.brand = brand;
        this.model = model;
        COUNT_ID++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
