package pattern_dao;

public interface CarDAO {

    Car create(String brand, String model);
    Car getById(int id);
    void update(Car car);
    void delete(int carId);
}
