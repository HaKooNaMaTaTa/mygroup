package exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

//2 способа обработки исключительных ситуаций
// 1. try-catch обработка
// 2. Воспользоваться ключевым словом throws в контракте метода
public class FileNotFoundEx {
    public static void main(String[] args) {
        someMethod();
    }

    public static void someMethod() {
        File file = new File("File.txt");
        //Между фигурных скобок оператора try мы помещаем код,
        //в котором у нас может возникнуть исключение
        try {
            Scanner scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (RuntimeException e) {
            System.out.println("Мы в рантайме");
        } catch (Exception e) {
            System.out.println("Мы в эксепшене");
        }
    }
}
