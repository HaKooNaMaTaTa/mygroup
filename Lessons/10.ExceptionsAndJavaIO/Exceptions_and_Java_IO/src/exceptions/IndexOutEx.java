package exceptions;

import java.util.Scanner;

public class IndexOutEx {
    public static void main(String[] args) {
        int[] array = {9, 1, 2, 65, 78, 124};

        for (int i = 0; i <= array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
