package exceptions;

import java.util.Scanner;

public class ArithmeticEx {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        int result = -1;
        if (number2 == 0) {
            number2 = 1;
        }
        try {
            result = number1 / number2;
        } catch (ArithmeticException e) {
            throw new ArithmeticException("Ну ты и валенок! Делить на ноль нельзя");
        }

        System.out.println(result);
    }
}
