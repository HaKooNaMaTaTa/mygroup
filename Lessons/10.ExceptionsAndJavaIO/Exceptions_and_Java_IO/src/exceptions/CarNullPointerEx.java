package exceptions;

public class CarNullPointerEx {
    public static void main(String[] args) {
        Car car = null;

        if (car != null) {
            car.someMethod();
        }
    }
}
