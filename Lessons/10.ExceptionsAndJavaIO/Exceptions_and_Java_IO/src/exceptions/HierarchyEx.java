package exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class HierarchyEx {
    public static void main(String[] args) {
        File file = new File("File.txt");

        try {
            Scanner scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (RuntimeException e) {
            System.out.println("Мы в рантайме");
        } catch (Exception e) {
            System.out.println("Мы в эксепшене");
        }
    }
}
