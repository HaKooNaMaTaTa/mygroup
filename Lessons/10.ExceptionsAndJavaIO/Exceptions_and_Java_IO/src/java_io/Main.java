package java_io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class Main {
    //Java IO (Input / Output) - библиотека для работы с потоками данных (ввода и вывода)
    //Поток данных - упорядоченная последовательность данных, которой соответствует
    //определенный источник
    public static void main(String[] args) throws IOException {
//        Reader reader = new BufferedReader(new FileReader("file.txt")); //абстрактный класс для работы со входящим потоком данных
//        Writer writer= new BufferedWriter(new FileWriter("file.txt")); //абстрактный класс для работы с исходящим потоком данных

        File file = new File("resources/file.txt");
        System.out.println("Попытка создания файла - " + file.createNewFile());

        //Если append - false -> значит файл, каждый раз будет перезаписываться, если true - то будет дополняться
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resources/file.txt", true));

        bufferedWriter.write("Igonin Oleg\n");
        bufferedWriter.write("Igonin Igor\n");
        bufferedWriter.write("Igonin Varvara\n");
        bufferedWriter.write("Igonin Vovan\n");
        bufferedWriter.flush();

        BufferedReader bufferedReader = new BufferedReader(new FileReader("resources/file.txt"));
        String stroke = bufferedReader.readLine();
        while (stroke != null) {
            System.out.println(stroke);
            stroke = bufferedReader.readLine();
        }
    }
}
