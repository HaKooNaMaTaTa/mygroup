package jUnit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class NumberUtilTest {

    //@Test - данный метод - тестовый
    @ParameterizedTest(name = "return <true> on {0}")
    @ValueSource(ints = {3, 7, 13, 17})
    @DisplayName("Проверка метода isSimple() на простых числах")
    void isSimpleTrue(int number) {
        assertTrue(NumberUtil.isSimple(number));
    }


    @ParameterizedTest(name = "return <false> on {0}")
    @ValueSource(ints = {4, 6, 128, 1542})
    @DisplayName("Проверка метода isSimple() на не простых числах")
    void isSimpleFalse(int number) {
        assertFalse(NumberUtil.isSimple(number));
    }
}