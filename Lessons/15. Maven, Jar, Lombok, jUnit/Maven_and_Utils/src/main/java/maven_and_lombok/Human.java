package maven_and_lombok;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Human {


    private String firstName;
    private String lastName;
    private String patronymic;
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private int age;

}
