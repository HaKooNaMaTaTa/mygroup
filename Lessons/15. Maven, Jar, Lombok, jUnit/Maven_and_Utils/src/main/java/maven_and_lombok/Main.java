package maven_and_lombok;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Audi", "A6", "Black");
        Human human = new Human("Oleg", "Igonin", "Leonidovich", 27);
        Human human1 = new Human("Oleg", "Igonin", "Leonidovich", 30);

        System.out.println(human);

        System.out.println(human.equals(human1));

        Human builder = Human.builder()
                .firstName("Petr")
                .patronymic("Nicolaevich")
                .build();

        System.out.println(builder);
    }
}
