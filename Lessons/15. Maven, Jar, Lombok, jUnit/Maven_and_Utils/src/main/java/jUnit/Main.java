package jUnit;

public class Main {
    public static void main(String[] args) {
//        System.out.println(NumberUtil.isSimple(0));
        System.out.println(NumberUtil.isSimple(1));
        System.out.println(NumberUtil.isSimple(3));
        System.out.println(NumberUtil.isSimple(13));
        System.out.println(NumberUtil.isSimple(15));
        System.out.println(NumberUtil.isSimple(169));

        if (!NumberUtil.isSimple(3)) {
            throw new RuntimeException("Ваш код не работает");
        }

    }
}
