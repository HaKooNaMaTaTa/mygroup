package jUnit;

public class NumberUtil {

    public static boolean isSimple(int number) {
        if (number == 0) {
            throw new RuntimeException("Зачем был передан ноль???");
        }

        if (number == 1 || number == 2) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }
}
