package spring_data.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring_data.config.ApplicationConfig;
import spring_data.dto.AccountDto;
import spring_data.model.Account;
import spring_data.service.AccountService;

import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);


		AccountService accountService = (AccountService) applicationContext.getBean("accountServiceImpl");

		List<Account> accounts = accountService.findAllByCarsColor("Red");

		int i = 0;
	}
}
