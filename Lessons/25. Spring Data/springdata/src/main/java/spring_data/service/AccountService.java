package spring_data.service;

import spring_data.dto.AccountDto;
import spring_data.model.Account;

import java.util.List;

public interface AccountService {

    void saveAccount(AccountDto accountDto);

    List<Account> getAllAccounts();

    List<Account> findAllByLastName(String lastName);

    List<Account> findAllByEmail(String email);

    List<Account> findAllByCarsColor(String color);
}
