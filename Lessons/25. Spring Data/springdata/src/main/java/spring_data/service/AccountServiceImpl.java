package spring_data.service;

import spring_data.dto.AccountDto;
import spring_data.model.Account;
import spring_data.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveAccount(AccountDto accountDto) {
        Account account = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail())
                .password(accountDto.getPassword())
                .build();

        accountRepository.save(account);

        System.out.println("ID account - " + account.getId());
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public List<Account> findAllByLastName(String lastName) {
        return accountRepository.findAllByLastName(lastName);
    }

    @Override
    public List<Account> findAllByEmail(String email) {
        return accountRepository.fBE(email);
    }

    @Override
    public List<Account> findAllByCarsColor(String color) {
        return accountRepository.findAllByCars_Color(color);
    }

}
