package spring_data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import spring_data.model.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

	List<Account> findAllByLastName(String lastName);

	//Таблица User, поле integration_some_attribute_id as integ_id
	//select integ_id
	@Query("select acc from Account acc where acc.email = :email")
	List<Account> fBE(@Param("email") String email);

	//Подсказка к вопросу о '_' в домашней задаче
	List<Account> findAllByCars_Color(String color);
}
