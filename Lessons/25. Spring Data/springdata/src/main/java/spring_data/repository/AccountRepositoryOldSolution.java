package spring_data.repository;

import spring_data.model.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepositoryOldSolution {

    void save(Account account);

    List<Account> getAllAccounts();
}
