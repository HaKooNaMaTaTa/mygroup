package app.main;

import app.config.ApplicationConfig;
import app.models.Account;
import app.services.SignUpService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SignUpService signUpService = (SignUpService) applicationContext.getBean("signUpServiceImpl");

//        Account account = Account.builder()
//                .firstName("Boooch")
//                .age(1)
//                .build();
//
//        signUpService.signUp(account);

        System.out.println(signUpService.findById(16L));
    }
}
