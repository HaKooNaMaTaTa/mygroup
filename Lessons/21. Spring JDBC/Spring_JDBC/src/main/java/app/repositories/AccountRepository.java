package app.repositories;

import app.models.Account;

import java.util.Optional;

public interface AccountRepository {
    void save(Account account);
    Optional<Account> findById(Long id);
}
