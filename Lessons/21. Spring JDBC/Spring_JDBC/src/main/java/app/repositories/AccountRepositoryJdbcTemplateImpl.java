package app.repositories;

import app.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class AccountRepositoryJdbcTemplateImpl implements AccountRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    //language=SQL
    private static final String SQL_INSERT = "insert into account(email, password, age, first_name, last_name, patronymic) " +
            "VALUES (:email, :password, :age, :firstName, :lastName, :patronymic)";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from account where id = :id";

    private static final RowMapper<Account> accountMapper = (row, rowNum) -> Account.builder()
            .id(row.getLong("id"))
            .lastName(row.getString("last_name"))
            .firstName(row.getString("first_name"))
            .patronymic(row.getString("patronymic"))
            .age(row.getInt("age"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();
    @Autowired
    public AccountRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(Account account) {
        Map<String, Object> params = new HashMap<>();
        params.put("email", account.getEmail());
        params.put("password", account.getPassword());
        params.put("age", account.getAge());
        params.put("firstName", account.getFirstName());
        params.put("lastName", account.getLastName());
        params.put("patronymic", account.getPatronymic());
        jdbcTemplate.update(SQL_INSERT, params);
    }

    @Override
    public Optional<Account> findById(Long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_FIND_BY_ID, Collections.singletonMap("id", id),accountMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
