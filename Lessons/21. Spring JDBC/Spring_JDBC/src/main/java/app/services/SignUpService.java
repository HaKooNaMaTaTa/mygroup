package app.services;

import app.models.Account;

import java.util.Optional;

public interface SignUpService {
    void signUp(Account account);
    Account findById(Long id);
}
