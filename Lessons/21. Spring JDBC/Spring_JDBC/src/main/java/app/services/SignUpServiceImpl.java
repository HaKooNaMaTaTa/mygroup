package app.services;

import app.models.Account;
import app.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;

    @Autowired
    public SignUpServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(Account account) {
        System.out.println("Начинаю подготовку базы данных, для сохранения аккаунта");
        System.out.println("Сохраняю аккаунт");
        accountRepository.save(account);
        System.out.println("Аккаунт сохранен");
    }

    @Override
    public Account findById(Long id) {
        Optional<Account> optAcc = accountRepository.findById(id);
        //Автоматически проверяется наличие объекта внутри обертки Optional
        //Если объект есть, то применяется метод .get() и возвращается сам объект
        //Если внутри обертки лежи null, То мы возвращаем то, что лежит в скобках метода
        //.orElse().
        return optAcc.orElse(Account.builder().build());
    }
}
