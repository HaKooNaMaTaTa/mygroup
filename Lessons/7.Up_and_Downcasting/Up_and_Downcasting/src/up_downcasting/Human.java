package up_downcasting;

public class Human extends Monkey {


    public Human() {
    }

    @Override
    public void eat() {
        System.out.println("Я эволюционировал в человека, но все равно ем банан");
    }

    public void goToWork() {
        System.out.println("Человек пошел на работу");
    }
}
