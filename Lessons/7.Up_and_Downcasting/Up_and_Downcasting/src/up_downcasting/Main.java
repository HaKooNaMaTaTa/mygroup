package up_downcasting;

public class Main {
    public static void main(String[] args) {
//        Animal animalMonkey = new Monkey();
//        Animal animalHuman = new Human();
//        Animal animalCat = new Cat();
//        Animal animalDog = new Dog();
//
//
//        byte _byte = 100;
//        int number = _byte;
//
//        int number2 = 140;
//        byte byte2 = (byte) number2;
//        //byte - 1 Байт
//        //short - 2 Байт

//        Animal animal = new Monkey();
//        Animal animal = new Animal();
//
//        animal.eat();
//
//        Monkey monkey = (Monkey) animal;
//
//        monkey.climbATree();
//        Cat cat = new Cat();
        Human human = new Human();
        someMethod(human);
    }

    //instanceof проверяет на основе чего создан объект
    public static void someMethod(Object object) {
        if (object instanceof Monkey) {
            Monkey monkey = (Monkey) object;
            monkey.climbATree();
        }
//
//        if (object instanceof Cat) {
//            Cat cat = (Cat) object;
//            cat.meeeeow();
//        }
    }
}


class Monkey {}

class Human extends Monkey {}

class Main {
    public static void main(String[] args) {
        Human human = new Human();
        someMethod(human);
    }
}