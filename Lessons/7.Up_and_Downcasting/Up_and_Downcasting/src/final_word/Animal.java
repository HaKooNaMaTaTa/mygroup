package final_word;

public class Animal {

    public String nickname;

    public Animal(String nickname) {
        this.nickname = nickname;
    }

    //final применяется к методу - то данный нельзя переопределить в классах наследниках
    //public final void eat() {
    public void eat() {
        System.out.println("Животное что то ест");
    }
}
