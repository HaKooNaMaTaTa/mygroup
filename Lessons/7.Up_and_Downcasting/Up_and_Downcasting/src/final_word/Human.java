package final_word;
//final у класса - запрещаем классу иметь наследников (наследоваться от класса)
//public final class Human extends Monkey {
public class Human extends Monkey {

    public Human() {
    }

    @Override
    public void eat() {
        System.out.println("Я эволюционировал в человека, но все равно ем банан");
    }

    public void goToWork() {
        System.out.println("Человек пошел на работу");
    }
}
