package final_word;

public class Monkey extends Animal {


    public Monkey() {
        super(null);
        climbATree();
        eat();
    }

    @Override
    public void eat() {
        System.out.println("Мартышка кушает банан");
    }

    public void climbATree() {
        System.out.println("Мартышка полезла на дерево");
    }

}
