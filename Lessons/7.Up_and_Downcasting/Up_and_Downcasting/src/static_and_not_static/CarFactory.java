package static_and_not_static;

public class CarFactory {
    //Ключевое слово static - делает так, что поле или метод теперь принадлежит не объекту, а классу
    static int currentId = 0;

    public static Car createCar() {
        Car car = new Car(currentId);
        currentId++;
        return car;
    }
}
