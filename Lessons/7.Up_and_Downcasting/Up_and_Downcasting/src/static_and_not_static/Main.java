package static_and_not_static;

public class Main {
    public static void main(String[] args) {
        sayHello();
        Car car = CarFactory.createCar();
        Car car1 = CarFactory.createCar();
        Car car2 = CarFactory.createCar();

        System.out.println(car.getId());
        System.out.println(car1.getId());
        System.out.println(car2.getId());
        System.out.println(CarFactory.currentId);

        System.out.println("А теперь автопром из Германии");
        Car carFromGermany = CarFactory.createCar();
        Car carFromGermany1 = CarFactory.createCar();
        Car carFromGermany2 = CarFactory.createCar();

        System.out.println(carFromGermany.getId());
        System.out.println(carFromGermany1.getId());
        System.out.println(carFromGermany2.getId());
        System.out.println(CarFactory.currentId);

    }

    public static void sayHello() {
        System.out.println("Hello");
    }

    public void sayGoodBye() {
        System.out.println("GoodBye");
        sayHello();
    }
}
